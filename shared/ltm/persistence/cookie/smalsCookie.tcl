#ENV ALL
ltm persistence cookie smalsCookie {
    always-send disabled
    app-service none
    cookie-encryption required
    cookie-encryption-passphrase [vault][f5/f5.shared/ltm/persistence/cookie/smalsCookie.tcl][cookieencryptionpassphrase]
    cookie-name none
    defaults-from none
    description "Managed by Git repo f5.shared - Version: : 6e5ab1ae45ec46ea516910db54ea35d614bd9644 $ - :Git ID: (%h) %ci - %cn(%ce)$"
    expiration 0
    hash-length 0
    hash-offset 0
    #httponly enabled
    match-across-pools disabled
    match-across-services disabled
    match-across-virtuals disabled
    method insert
    mirror disabled
    override-connection-limit disabled
    partition Common
    #secure enabled
    timeout 180
}
