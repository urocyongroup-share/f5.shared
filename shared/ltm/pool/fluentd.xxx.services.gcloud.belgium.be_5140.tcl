#ENV prd 
ltm pool /Common/fluentd.xxx.services.gcloud.belgium.be_5140 {
    allow-nat yes
    allow-snat yes
    app-service none
    description "Managed by Git repo f5.shared - Version: : 6e5ab1ae45ec46ea516910db54ea35d614bd9644 $ - :Git ID: (%h) %ci - %cn(%ce)$"
    gateway-failsafe-device none
    ignore-persisted-weight disabled
    ip-tos-to-client pass-through
    ip-tos-to-server pass-through
    link-qos-to-client pass-through
    link-qos-to-server pass-through
    load-balancing-mode round-robin
    members {
        fluentdA1.xxx.services.gcloud.belgium.be:5140 {
            address _fluentdA1.xxx.services.gcloud.belgium.be_
            app-service none
            connection-limit 0
            description "Managed by Git repo f5.shared - Version: : 6e5ab1ae45ec46ea516910db54ea35d614bd9644 $ - :Git ID: (%h) %ci - %cn(%ce)$"
            dynamic-ratio 1
            inherit-profile enabled
            logging disabled
            monitor default
            priority-group 0
            rate-limit disabled
            ratio 1
            session monitor-enabled
            state up
            fqdn {
                autopopulate disabled
                name none
            }
            metadata none
            profiles none
        }
        fluentdB1.xxx.services.gcloud.belgium.be:5140 {
            address _fluentdB1.xxx.services.gcloud.belgium.be_
            app-service none
            connection-limit 0
            description "Managed by Git repo f5.shared - Version: : 6e5ab1ae45ec46ea516910db54ea35d614bd9644 $ - :Git ID: (%h) %ci - %cn(%ce)$"
            dynamic-ratio 1
            inherit-profile enabled
            logging disabled
            monitor default
            priority-group 0
            rate-limit disabled
            ratio 1
            session monitor-enabled
            state up
            fqdn {
                autopopulate disabled
                name none
            }
            metadata none
            profiles none
        }
    }
    metadata none
    min-active-members 0
    min-up-members 0
    min-up-members-action failover
    min-up-members-checking disabled
    monitor udp
    partition Common
    profiles none
    queue-depth-limit 0
    queue-on-connection-limit disabled
    queue-time-limit 0
    reselect-tries 0
    service-down-action reselect
    slow-ramp-time 10
}
