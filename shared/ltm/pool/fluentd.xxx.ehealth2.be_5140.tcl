#ENV bcp 
ltm pool /EH2/fluentd.xxx.ehealth2.be_5140 {
    allow-nat yes
    allow-snat yes
    app-service none
    description "Managed by Git repo f5.shared - Version: : 6e5ab1ae45ec46ea516910db54ea35d614bd9644 $ - :Git ID: (%h) %ci - %cn(%ce)$"
    gateway-failsafe-device none
    ignore-persisted-weight disabled
    ip-tos-to-client pass-through
    ip-tos-to-server pass-through
    link-qos-to-client pass-through
    link-qos-to-server pass-through
    load-balancing-mode round-robin
    members {
        fluentdA1.xxx.ehealth2.be:5140 {
            address _fluentdA1.xxx.ehealth2.be_%31
            app-service none
            connection-limit 0
            description "Managed by Git repo f5.shared - Version: : 6e5ab1ae45ec46ea516910db54ea35d614bd9644 $ - :Git ID: (%h) %ci - %cn(%ce)$"
            dynamic-ratio 1
            inherit-profile enabled
            logging disabled
            monitor default
            priority-group 0
            rate-limit disabled
            ratio 1
            session monitor-enabled
            state up
            fqdn {
                autopopulate disabled
                name none
            }
            metadata none
            profiles none
        }
    }
    metadata none
    min-active-members 0
    min-up-members 0
    min-up-members-action failover
    min-up-members-checking disabled
    monitor udp
    partition EH2
    profiles none
    queue-depth-limit 0
    queue-on-connection-limit disabled
    queue-time-limit 0
    reselect-tries 0
    service-down-action reselect
    slow-ramp-time 10
}
