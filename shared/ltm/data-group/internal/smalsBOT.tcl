#ENV ALL
ltm data-group internal smalsBOT {
    app-service none
    description "List of know BOT ip's - https://isc.sans.edu/block.txt"
    partition Common
    records {
        91.223.133.0/24 {
            data "NO_N"
        }
        185.35.62.0/24 {
            data "KS-ASN1"
        }
        80.82.77.0/24 {
            data "QUASINETWORKS"
        }
        60.191.38.0/24 {
            data "CHINANET-BACKBONE"
        }
        89.248.172.0/24 {
            data "QUASINETWORKS"
        }
        74.82.47.0/24 {
            data "HURRICANE"
        }
        45.55.4.0/24 {
            data "DIGITALOCEAN-ASN"
        }
        141.212.122.0/24 {
            data "UMICH-AS-5"
        }
        180.97.106.0/24 {
            data "CHINANET-BACKBONE"
        }
        204.42.253.0/24 {
            data "NTT-COMMUNICATIONS-2914"
        }
        104.193.252.0/24 {
            data "HOSTING-SOLUTIONS"
        }
        64.125.239.0/24 {
            data "ZAYO-6461"
        }
    }
    type ip
}
