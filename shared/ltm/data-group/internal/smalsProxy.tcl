#ENV ALL
ltm data-group internal smalsProxy {
    app-service none
    description "Proxies for which we can trust X-Forwarded-For"
    partition Common
    records {
        85.91.175.213/32 {
            data "proxyuser Blue Coat"
        }
        85.91.175.221/32 {
            data "proxyuser Blue Coat"
        }
        85.91.175.222/32 {
            data "proxyuser Blue Coat"
        }
        2a01:690:7:100::2:afd5/128 {
            data "proxyuser Blue Coat"
        }
        2a01:690:7:100::2:afdd/128 {
            data "proxyuser Blue Coat"
        }
        2a01:690:7:100::2:afde/128 {
            data "proxyuser Blue Coat"
        }
    }
    type ip
}
