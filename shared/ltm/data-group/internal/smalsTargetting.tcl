#ENV ALL
#https://www.gcloud.belgium.be/doc/PAAS/network_zones/
ltm data-group internal smalsTargetting {
    app-service none
    description "Map known Smals IP ranges to their use type"
    partition Common
    records {
        100.64.88.64/26 {
            data "Gcloud services lab"
        }
        100.64.85.192/26 {
            data "Gcloud services tst"
        }
        100.64.85.128/26 {
            data "Gcloud services prd"
        }
        85.91.160.0/19 {
            data "smals ripe"
        }
        85.91.162.1 {
            data "smals hide sc"
        }
    }
    type ip
}
