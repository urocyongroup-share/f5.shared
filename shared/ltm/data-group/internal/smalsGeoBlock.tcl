#ENV ALL
ltm data-group internal smalsGeoBlock {
    app-service none
    description "Geo blocking based on country code - ISO 3166-1 alpha-2 code"
    partition Common
    records {
#        AF {
#            data "Afghanistan"
#        }
#        AX {
#            data "Åland Islands"
#        }
#        AL {
#            data "Albania"
#        }
#        DZ {
#            data "Algeria"
#        }
#        AS {
#            data "American Samoa"
#        }
#        AD {
#            data "Andorra"
#        }
#        AO {
#            data "Angola"
#        }
#        AI {
#            data "Anguilla"
#        }
        AQ {
            data "Antarctica"
        }
#        AG {
#            data "Antigua and Barbuda"
#        }
#        AR {
#            data "Argentina"
#        }
#        AM {
#            data "Armenia"
#        }
#        AW {
#            data "Aruba"
#        }
#        AU {
#            data "Australia"
#        }
#        AT {
#            data "Austria"
#        }
#        AZ {
#            data "Azerbaijan"
#        }
#        BS {
#            data "Bahamas"
#        }
#        BH {
#            data "Bahrain"
#        }
#        BD {
#            data "Bangladesh"
#        }
#        BB {
#            data "Barbados"
#        }
#        BY {
#            data "Belarus"
#        }
#        BE {
#            data "Belgium"
#        }
#        BZ {
#            data "Belize"
#        }
#        BJ {
#            data "Benin"
#        }
#        BM {
#            data "Bermuda"
#        }
#        BT {
#            data "Bhutan"
#        }
#        BO {
#            data "Bolivia, Plurinational State of"
#        }
#        BQ {
#            data "Bonaire, Sint Eustatius and Saba"
#        }
#        BA {
#            data "Bosnia and Herzegovina"
#        }
#        BW {
#            data "Botswana"
#        }
#        BV {
#            data "Bouvet Island"
#        }
#        BR {
#            data "Brazil"
#        }
#        IO {
#            data "British Indian Ocean Territory"
#        }
#        BN {
#            data "Brunei Darussalam"
#        }
#        BG {
#            data "Bulgaria"
#        }
#        BF {
#            data "Burkina Faso"
#        }
#        BI {
#            data "Burundi"
#        }
#        KH {
#            data "Cambodia"
#        }
#        CM {
#            data "Cameroon"
#        }
#        CA {
#            data "Canada"
#        }
#        CV {
#            data "Cape Verde"
#        }
#        KY {
#            data "Cayman Islands"
#        }
#        CF {
#            data "Central African Republic"
#        }
#        TD {
#            data "Chad"
#        }
#        CL {
#            data "Chile"
#        }
#        CN {
#            data "China"
#        }
#        CX {
#            data "Christmas Island"
#        }
#        CC {
#            data "Cocos (Keeling) Islands"
#        }
#        CO {
#            data "Colombia"
#        }
#        KM {
#            data "Comoros"
#        }
#        CG {
#            data "Congo"
#        }
#        CD {
#            data "Congo, The Democratic Republic of the"
#        }
#        CK {
#            data "Cook Islands"
#        }
#        CR {
#            data "Costa Rica"
#        }
#        CI {
#            data "Côte D'Ivoire"
#        }
#        HR {
#            data "Croatia"
#        }
#        CU {
#            data "Cuba"
#        }
#        CW {
#            data "Curaçao"
#        }
#        CY {
#            data "Cyprus"
#        }
#        CZ {
#            data "Czech Republic"
#        }
#        DK {
#            data "Denmark"
#        }
#        DJ {
#            data "Djibouti"
#        }
#        DM {
#            data "Dominica"
#        }
#        DO {
#            data "Dominican Republic"
#        }
#        EC {
#            data "Ecuador"
#        }
#        EG {
#            data "Egypt"
#        }
#        SV {
#            data "El Salvador"
#        }
#        GQ {
#            data "Equatorial Guinea"
#        }
#        ER {
#            data "Eritrea"
#        }
#        EE {
#            data "Estonia"
#        }
#        ET {
#            data "Ethiopia"
#        }
#        FK {
#            data "Falkland Islands (Malvinas)"
#        }
#        FO {
#            data "Faroe Islands"
#        }
#        FJ {
#            data "Fiji"
#        }
#        FI {
#            data "Finland"
#        }
#        FR {
#            data "France"
#        }
#        GF {
#            data "French Guiana"
#        }
#        PF {
#            data "French Polynesia"
#        }
#        TF {
#            data "French Southern Territories"
#        }
#        GA {
#            data "Gabon"
#        }
#        GM {
#            data "Gambia"
#        }
#        GE {
#            data "Georgia"
#        }
#        DE {
#            data "Germany"
#        }
#        GH {
#            data "Ghana"
#        }
#        GI {
#            data "Gibraltar"
#        }
#        GR {
#            data "Greece"
#        }
#        GL {
#            data "Greenland"
#        }
#        GD {
#            data "Grenada"
#        }
#        GP {
#            data "Guadeloupe"
#        }
#        GU {
#            data "Guam"
#        }
#        GT {
#            data "Guatemala"
#        }
#        GG {
#            data "Guernsey"
#        }
#        GN {
#            data "Guinea"
#        }
#        GW {
#            data "Guinea-Bissau"
#        }
#        GY {
#            data "Guyana"
#        }
#        HT {
#            data "Haiti"
#        }
#        HM {
#            data "Heard Island and Mcdonald Islands"
#        }
#        VA {
#            data "Holy See (Vatican City State)"
#        }
#        HN {
#            data "Honduras"
#        }
#        HK {
#            data "Hong Kong"
#        }
#        HU {
#            data "Hungary"
#        }
#        IS {
#            data "Iceland"
#        }
#        IN {
#            data "India"
#        }
#        ID {
#            data "Indonesia"
#        }
#        IR {
#            data "Iran, Islamic Republic of"
#        }
#        IQ {
#            data "Iraq"
#        }
#        IE {
#            data "Ireland"
#        }
#        IM {
#            data "Isle of Man"
#        }
#        IL {
#            data "Israel"
#        }
#        IT {
#            data "Italy"
#        }
#        JM {
#            data "Jamaica"
#        }
#        JP {
#            data "Japan"
#        }
#        JE {
#            data "Jersey"
#        }
#        JO {
#            data "Jordan"
#        }
#        KZ {
#            data "Kazakhstan"
#        }
#        KE {
#            data "Kenya"
#        }
#        KI {
#            data "Kiribati"
#        }
#        KP {
#            data "Korea, Democratic People's Republic of"
#        }
#        KR {
#            data "Korea, Republic of"
#        }
#        KW {
#            data "Kuwait"
#        }
#        KG {
#            data "Kyrgyzstan"
#        }
#        LA {
#            data "Lao People's Democratic Republic"
#        }
#        LV {
#            data "Latvia"
#        }
#        LB {
#            data "Lebanon"
#        }
#        LS {
#            data "Lesotho"
#        }
#        LR {
#            data "Liberia"
#        }
#        LY {
#            data "Libya"
#        }
#        LI {
#            data "Liechtenstein"
#        }
#        LT {
#            data "Lithuania"
#        }
#        LU {
#            data "Luxembourg"
#        }
#        MO {
#            data "Macao"
#        }
#        MK {
#            data "Macedonia, The former Yugoslav Republic of"
#        }
#        MG {
#            data "Madagascar"
#        }
#        MW {
#            data "Malawi"
#        }
#        MY {
#            data "Malaysia"
#        }
#        MV {
#            data "Maldives"
#        }
#        ML {
#            data "Mali"
#        }
#        MT {
#            data "Malta"
#        }
#        MH {
#            data "Marshall Islands"
#        }
#        MQ {
#            data "Martinique"
#        }
#        MR {
#            data "Mauritania"
#        }
#        MU {
#            data "Mauritius"
#        }
#        YT {
#            data "Mayotte"
#        }
#        MX {
#            data "Mexico"
#        }
#        FM {
#            data "Micronesia, Federated States of"
#        }
#        MD {
#            data "Moldova, Republic of"
#        }
#        MC {
#            data "Monaco"
#        }
#        MN {
#            data "Mongolia"
#        }
#        ME {
#            data "Montenegro"
#        }
#         MS {
#            data "Montserrat"
#        }
#        MA {
#            data "Morocco"
#        }
#        MZ {
#            data "Mozambique"
#        }
#        MM {
#            data "Myanmar"
#        }
#        NA {
#            data "Namibia"
#        }
#        NR {
#            data "Nauru"
#        }
#        NP {
#            data "Nepal"
#        }
#        NL {
#            data "Netherlands"
#        }
#        NC {
#            data "New Caledonia"
#        }
#        NZ {
#            data "New Zealand"
#        }
#        NI {
#            data "Nicaragua"
#        }
#        NE {
#            data "Niger"
#        }
#        NG {
#            data "Nigeria"
#        }
#        NU {
#            data "Niue"
#        }
#        NF {
#            data "Norfolk Island"
#        }
#        MP {
#            data "Northern Mariana Islands"
#        }
#        NO {
#            data "Norway"
#        }
#        OM {
#            data "Oman"
#        }
#        PK {
#            data "Pakistan"
#        }
#        PW {
#            data "Palau"
#        }
#        PS {
#            data "Palestine, State of"
#        }
#        PA {
#            data "Panama"
#        }
#        PG{
#            data "Papua New Guinea"
#        }
#        PY{
#            data "Paraguay"
#        }
#        PE{
#            data "Peru"
#        }
#        PH{
#            data "Philippines"
#        }
#        PN {
#            data "Pitcairn"
#        }
#        PL {
#            data "Poland"
#        }
#        PT {
#            data "Portugal"
#        }
#        PR {
#            data "Puerto Rico"
#        }
#        QA {
#            data "Qatar"
#        }
#        RE {
#            data "Réunion"
#        }
#        RO {
#            data "Romania"
#        }
#        RU {
#            data "Russian Federation"
#        }
#        RW {
#            data "Rwanda"
#        }
#        BL {
#            data "Saint Barthélemy"
#        }
#        SH {
#            data "Saint Helena, Ascension and Tristan da Cunha"
#        }
#        KN {
#            data "Saint Kitts and Nevis"
#        }
#        LC {
#            data "Saint Lucia"
#        }
#        MF {
#            data "Saint Martin (French Part)"
#        }
#        PM {
#            data "Saint Pierre and Miquelon"
#        }
#        VC {
#            data "Saint Vincent and the Grenadines"
#        }
#        WS {
#            data "Samoa"
#        }
#        SM {
#            data "San Marino"
#        }
#        ST {
#            data "Sao Tome and Principe"
#        }
#        SA {
#            data "Saudi Arabia"
#        }
#        SN {
#            data "Senegal"
#        }
#        RS {
#            data "Serbia"
#        }
#        SC {
#            data "Seychelles"
#        }
#        SL {
#            data "Sierra Leone"
#        }
#        SG {
#            data "Singapore"
#        }
#        SX {
#            data "Sint Maarten (Dutch Part)"
#        }
#        SK {
#            data "Slovakia"
#        }
#        SI {
#            data "Slovenia"
#        }
#        SB {
#            data "Solomon Islands"
#        }
#        SO {
#            data "Somalia"
#        }
#        ZA {
#            data "South Africa"
#        }
#        GS {
#            data "South Georgia and the South Sandwich Islands"
#        }
#        SS {
#            data "South Sudan"
#        }
#        ES {
#            data "Spain"
#        }
#        LK {
#            data "Sri Lanka"
#        }
#        SD {
#            data "Sudan"
#        }
#        SR {
#            data "Suriname"
#        }
#        SJ {
#            data "Svalbard and Jan Mayen"
#        }
#        SZ {
#            data "Swaziland"
#        }
#        SE {
#            data "Sweden"
#        }
#        CH {
#            data "Switzerland"
#        }
#        SY {
#            data "Syrian Arab Republic"
#        }
#        TW {
#            data "Taiwan, Province of China"
#        }
#        TJ {
#            data "Tajikistan"
#        }
#        TZ {
#            data "Tanzania, United Republic of"
#        }
#        TH {
#            data "Thailand"
#        }
#        TL {
#            data "Timor-Leste"
#        }
#        TG {
#            data "Togo"
#        }
#        TK {
#            data "Tokelau"
#        }
#        TO {
#            data "Tonga"
#        }
#        TT {
#            data "Trinidad and Tobago"
#        }
#        TN {
#            data "Tunisia"
#        }
#        TR {
#            data "Turkey"
#        }
#        TM {
#            data "Turkmenistan"
#        }
#        TC {
#            data "Turks and Caicos Islands"
#        }
#        TV {
#            data "Tuvalu"
#        }
#        UG {
#            data "Uganda"
#        }
#        UA {
#            data "Ukraine"
#        }
#        AE {
#            data "United Arab Emirates"
#        }
#        GB {
#            data "United Kingdom"
#        }
#        US {
#            data "United States"
#        }
#        UM {
#            data "United States Minor Outlying Islands"
#        }
#        UY {
#            data "Uruguay"
#        }
#        UZ {
#            data "Uzbekistan"
#        }
#        VU {
#            data "Vanuatu"
#        }
#        VE {
#            data "Venezuela, Bolivarian Republic of"
#        }
#        VN {
#            data "Viet Nam"
#        }
#        VG {
#            data "Virgin Islands, British"
#        }
#        VI {
#            data "Virgin Islands, U.S."
#        }
#        WF {
#            data "Wallis and Futuna"
#        }
#        EH {
#            data "Western Sahara"
#        }
#        YE {
#            data "Yemen"
#        }
#        ZM {
#            data "Zambia"
#        }
#        ZW {
#            data "Zimbabwe"
#        }
    }
    type string
}
