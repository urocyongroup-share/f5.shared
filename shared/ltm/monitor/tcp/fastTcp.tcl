#ENV ALL
ltm monitor tcp fastTcp {
    #adaptive disabled
    #adaptive-divergence-type relative
    #adaptive-divergence-value 25
    #adaptive-limit 200
    #adaptive-sampling-timespan 300
    app-service none
    defaults-from none
    description "Managed by Git repo f5.shared - Version: : 6e5ab1ae45ec46ea516910db54ea35d614bd9644 $ - :Git ID: (%h) %ci - %cn(%ce)$"
    destination *:*
    interval 1
    ip-dscp 0
    manual-resume disabled
    partition Common
    recv none
    recv-disable none
    reverse disabled
    send none
    time-until-up 0
    timeout 16
    transparent disabled
    up-interval 3
}
