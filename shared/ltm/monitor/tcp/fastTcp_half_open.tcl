#ENV ALL
ltm monitor tcp-half-open fastTcp_half_open {
    app-service none
    defaults-from tcp_half_open
    description none
    destination *:*
    interval 1
    manual-resume disabled
    partition Common
    time-until-up 0
    timeout 16
    transparent disabled
    up-interval 0
}
