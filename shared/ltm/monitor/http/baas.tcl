#ENV ALL
ltm monitor http baas {
    #adaptive disabled
    #adaptive-divergence-type relative
    #adaptive-divergence-value 25
    #adaptive-limit 200
    #adaptive-sampling-timespan 300
    app-service none
    defaults-from none
    description "Managed by Git repo f5.shared - Version: : 6e5ab1ae45ec46ea516910db54ea35d614bd9644 $ - :Git ID: (%h) %ci - %cn(%ce)$"
    destination *:*
    interval 1
    ip-dscp 0
    manual-resume disabled
    partition Common
    password none
    recv HTTP\/1\.[10]\s200
    recv-disable none
    reverse disabled
    send "GET /webconsole/api HTTP/1.0\r\n\r\n"
    time-until-up 0
    timeout 16
    transparent disabled
    up-interval 3
    username none
}
