#ENV ALL
ltm rule smalsSecurity {
# Managed by Git (reponname)
# Version: $Id$
# $Format:Git ID: (%h) %ci/%cn$


when RULE_INIT {
  set static::mseconds 10000
  set static::maxdupreq 10
  }
when CLIENT_ACCEPTED priority 10 {
  set dup_req 0
  set last_req ""

  ##### mitigate Slowloris attacks ####
  # Set an initial false value for $rtimer
  set rtimer 0
  # Execute this block after 1 second
  after 1000 {
    # If $rtimer hasn't been set to true then drop the connection
    if {not $rtimer} {
      drop
    }
  }
  #####################################

  ############# GEO BLOCK #############
  # Evaluate the client's country code against a data group
  # containing the country codes we want to block
  if { [class match [whereis [IP::client_addr] country] equals  smalsGeoBlock] } {
    # Optionally log a message that the client connection was dropped
    # Disable or use High Speed Logging if actually under attack
    log "Dropping connection based on geo block in datagroup smalsGeoBlock from client: [IP::client_addr], country code: [whereis [IP::client_addr] country]"
    ### Drop the connection if a country code match was located
    drop
    }
  #####################################
  }

when HTTP_REQUEST priority 10 {

  ############ IP repuation ###########
  # Use [HTTP::header values "X-Forwarded-For"] in replacement of [IP::client_addr] if using the x-forwarded-for testing
  set iprep_categories [IP::reputation [IP::client_addr]]
  set is_reject 0

  if { $iprep_categories contains "Windows Exploits" } {
    set is_reject 1
  }
  if { $iprep_categories contains "Web Attacks" } { 
    set is_reject 1
  }
  if { $iprep_categories contains "Scanners" } { 
    set is_reject 1
  }
  if { $iprep_categories contains "Proxy" } { 
    set is_reject 1
  }

  if { $is_reject } {
    log local0. "Attempted access from malicious IP address [IP::client_addr]($iprep_categories) - rejected"
    HTTP::respond 200 content "<HTML><HEAD><TITLE>Rejected Request</TITLE></HEAD>
	<BODY>The request was rejected. <BR>Attempted access from malicious IP address</BODY>
        </HTML>"
  }
  #####################################


  ##### mitigate Slowloris attacks ####
  # Set $rtimer to true to indicate that 
  # we have received a HTTP complete request
  set rtimer 1
  #####################################

  # Evaluate BOT list
  if { [class match "$ipClient" equals smalsBOT] } {
    set isBOT true
  } else {
    set isBOT false
  }

  # BOT strikeback [https://devcentral.f5.com/articles/mirai-strikeback-an-irule-to-kill-iot-bot-processes-from-your-f5-22644]
  # - Kill bot using bad location header
  # - The best defense is a good offense
  if { $isBOT } {
    if { $last_req equals "" } {
      set last_req [HTTP::uri]
      set dup_req 0
    }
    elseif { $last_req == [HTTP::uri] } {
      incr dup_req
      after $static::mseconds { if {$dup_req > 0} {incr dup_req -1} }
      if { $dup_req > $static::maxdupreq } {
        log "Killing suspected Bot at [IP::client_addr]"
        TCP::respond "HTTP/1.0\r\n200 OK\r\nLocation: http\r\n\r\n"
        TCP::close
      }
    }
    else {
      set dup_req 0
    }
  }


}

when HTTP_RESPONSE priority 10 {

  ########### Heist attacks ###########
  # - https://www.blackhat.com/docs/us-16/materials/us-16-VanGoethem-HEIST-HTTP-Encrypted-Information-Can-Be-Stolen-Through-TCP-Windows-wp.pdf
  set sRandomString {}
  set iLength [expr {int(rand() * 1000 + 1)}]
  for {set i 0} {$i < $iLength} {incr i} {
    set iRandomNum [expr {int(rand() * 62)}]
    if {$iRandomNum < 10} {
      incr iRandomNum 48
    } elseif {$iRandomNum < 36} {
      incr iRandomNum 55
    } else {
      incr iRandomNum 61
    }
    append sRandomString [format %c $iRandomNum]
  }

  HTTP::header insert X-SMALS-HEIST $sRandomString
  log local0. "Added random header X-HEIST: $sRandomString"
  #####################################
}

app-service none
partition Common
}

