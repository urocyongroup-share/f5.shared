#ENV ALL
ltm rule smalsHttpWebservice {
# Managed by Git (reponname)
# Version: $Id$
# $Format:Git ID: (%h) %ci/%cn$

when HTTP_REQUEST priority 6 {
  if { [HTTP::header values "SOAPAction"] equals "\"\"" } {
      HTTP::header replace "SOAPAction" "None"
  }
  if { !([HTTP::header exists "SOAPAction"]) } {
      HTTP::header insert "SOAPAction" "None"
  }

  #https://jira.smals.be/browse/MIGRATIONSOA-72
  #call smalsProcs::regex_map  [HTTP::uri] {Var[A-D]\/v} {\/v}
  #HTTP::uri [string map -nocase {Var[A-D]\/v \/v} [HTTP::uri]]
  HTTP::uri [string map -nocase {VarA/v /v} [HTTP::uri]]
  HTTP::uri [string map -nocase {VarB/v /v} [HTTP::uri]]
  HTTP::uri [string map -nocase {VarC/v /v} [HTTP::uri]]
  HTTP::uri [string map -nocase {VarD/v /v} [HTTP::uri]]
}

app-service none
partition Common
}
