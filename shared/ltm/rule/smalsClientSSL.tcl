#ENV ALL
ltm rule smalsClientSsl {
# Managed by Git repo f5.shared
# Version: $Id$
# $Format:Git ID: (%h) %ci/%cn$

when CLIENT_ACCEPTED priority 15 {
  # Triggered when a client has established a connection.

  set sslRenegTried false
  set sslHTTPCollecting false
}

when CLIENTSSL_CLIENTHELLO priority 15 {
  # Triggered when the system has received the client's SSL ClientHello message

  # Set SSL traffic type marker
  set isClientSsl true

  # Initialize var
  set sslClientCert ""
  set sslClientCertCount 0
  set sslClientCertStatusCode ""
  set sslClientCertStatusDesc ""
}

when CLIENTSSL_HANDSHAKE priority 15 {
  # Triggered when a client-side SSL handshake is completed.

  # Get SSL properties
  set sslClientCipherName [SSL::cipher name]
  set sslClientCipherVersion [SSL::cipher version]
  set sslClientCipherBits [SSL::cipher bits]
  if { [SSL::cert count] ne 0 } {
    set sslClientCert [string map {"-----BEGIN CERTIFICATE-----" "" "-----END CERTIFICATE-----" "" "\n" ""} [X509::whole [SSL::cert 0]]]
    set sslClientCertStatusCode [SSL::verify_result]
    set sslClientCertStatusDesc [X509::verify_cert_error_string [SSL::verify_result]]
  }
  # Release HTTP request in case we are buffering
  if { $sslHTTPCollecting == true } {
    HTTP::release
  }
}

app-service none
partition Common
}
