#ENV ALL
ltm rule smalsLb {
# Managed by Git repo f5.shared
# Version: $Id$
# $Format:Git ID: (%h) %ci/%cn$

when LB_FAILED priority 4 {
  # Triggered when the system fails to select a pool or a pool member
  # or when a selected resource is unreachable.
  set lbPool "-"
  set lbMember "-"
  # Uncomment to enable debugging
  #call smalsProcs::sendLog "lgsF5Sys" "$vsName [LB::server] [event info]"
  set lbServer [LB::server]
}

when LB_SELECTED priority 4 {
  # Triggered when the system selects a pool member.

  set lbPool [LB::server pool]
  set lbMember "[lindex [split [LB::server addr] %] 0]:[LB::server port]"
  if { $lbMember eq ":" } {
    set lbMember "0.0.0.0:0"
  }
}

app-service none
partition Common
}
