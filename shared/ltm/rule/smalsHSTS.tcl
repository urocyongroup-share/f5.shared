#ENV ALL
ltm rule smalsHSTS {
# Managed by Git (reponname)
# Version: $Id$
# Git ID: (47126a6) 2017-11-16 10:10:09 +0100/Niels Vanderbeke

# Managed by Git (reponname)
# Version: $Id$
# Git ID: (47126a6) 2017-11-16 10:10:09 +0100/Niels Vanderbeke

when HTTP_REQUEST priority 7 {
  set partitions {Common EH1 EH2 EHB EHD Common SMA SOC SSB VAS AWPEH2 FMW IAP KSZ}
  foreach partition $partitions {
    if { [class exists /$partition/smalsHSTS] } {
      if { [class match "$httpHost" equals /$partition/smalsHSTS] } {
        set smalsHSTSenabled true
        set smalsHSTSoptions [class lookup "$httpHost" smalsHSTS]
      } else {
        set smalsHSTSenabled false
        set smalsHSTSoptions ""
      }
    }
  }

}


when HTTP_RESPONSE priority 7 {

  if { $smalsHSTSenabled } {
    HTTP::header insert Strict-Transport-Security "$smalsHSTSoptions"
  }

}



app-service none
partition Common
}
