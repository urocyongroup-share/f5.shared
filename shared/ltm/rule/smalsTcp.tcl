#ENV ALL
ltm rule smalsTcp {
# Managed by Git repo f5.shared
# Version: $Id$
# $Format:Git ID: (%h) %ci/%cn$

when CLIENT_ACCEPTED priority 9 {
  # Triggered when a client has established a connection.

  set ipLocalPort [TCP::local_port]
  set ipClientPort [TCP::client_port]
  set ipRemotePort [TCP::remote_port]
}

when CLIENT_CLOSED priority 9 {
  # This event is fired at the end of any client connection. regardless of protocol.

}

when SERVER_CONNECTED priority 9 {
  # Triggered when a connection has been established with the target node.
  set ipServerPort [TCP::server_port]
}

when SERVER_CLOSED priority 9 {
  # This event is triggered when the server side connection closes.

}

app-service none
partition Common
}
