#ENV ALL
ltm rule smalsIp {
# Managed by Git repo f5.shared
# Version: $Id$
# $Format:Git ID: (%h) %ci/%cn$

when CLIENT_ACCEPTED priority 3 {
  # Triggered when a client has established a connection.

  set isHttp false
  set isClientSsl false
  set isClientSslReneg false
  set isServerSsl false 

  #set the virutal servername
  set vsName [virtual name]
  # set the partition name where the virtual server lives
  set partitionName [lindex [split [virtual name] /] 1]

  set ipVersion [IP::version]
  set ipLocal [lindex [split [IP::local_addr] %] 0]
  set ipClient [lindex [split [IP::client_addr] %] 0]
  set ipLocalPort 0
  set ipClientPort 0

  # returns information on the SNAT configuration for the current connection.
  # if no snat is active the value should be "none"
  set lbSnat [LB::snat]
}

when CLIENT_CLOSED priority 3 {
  # This event is fired at the end of any client connection. regardless of protocol.

}

when SERVER_CONNECTED priority 3 {
  # Triggered when a connection has been established with the target node.

  set ipServer [lindex [split [IP::server_addr] %] 0]
  set ipServerPort 0
}

when SERVER_CLOSED priority 3 {
  # This event is triggered when the server side connection closes.

}

app-service none
partition Common
}
