#ENV ALL
ltm rule smalsHttp {
# Managed by Git (reponname)
# Version: $Id$
# $Format:Git ID: (%h) %ci/%cn$

when HTTP_REQUEST priority 5 {
  # Triggered when the system fully parses the complete client HTTP request headers.

  set isHttp true

  # Detect NTLM authentication
  set isNTLM false
  if { [HTTP::header values Authorization] contains "NTLM" } {
    set isNTLM true
  }

  # Avoid session persistence being ignored for Keep-Alive connections
  # https://support.f5.com/csp/article/K7964
  if {[HTTP::cookie names] contains "BIGipServer" and not $isNTLM }{
      catch {LB::detach}
  }
 
  # Evaluate targetting
  if { [class match "$ipClient" equals smalsTargetting] } {
    set smalsClientIp true
    set smalsClientIpType [class lookup "$ipClient" smalsTargetting]
  } else {
    set smalsClientIp false
    set smalsClientIpType ""
  }

  # Evaluate proxy
  if { [class match "$ipClient" equals smalsProxy] } {
    set isTrustedProxy true
  } else {
    set isTrustedProxy false
  }

  if { $isClientSsl } {
    set sslClientCipherNameLog "$sslClientCipherName"
    set sslClientCipherVersionLog "$sslClientCipherVersion"
    set sslClientCipherBitsLog "$sslClientCipherBits"
  } else {
    set sslClientCipherNameLog "-"
    set sslClientCipherVersionLog "-"
    set sslClientCipherBitsLog "-"
  }

  # Evaluate policy enforcement point (Webgate)
#  if { [class match "$ipClient" equals smalsPEP] } {
#    set isPEP true
#  } else {
    set isPEP false
#  }

  # Get or generate httpHost
  if { [HTTP::host] eq "" } {
    set httpHost [string tolower "[lindex [split "$ipLocal" %] 0]:$ipLocalPort"]
  } else {
    set httpHost [string tolower "[getfield [HTTP::host] : 1]"]
  }

  set httpVersion [HTTP::version]
  set httpMethod [HTTP::method]
  set httpPath [HTTP::path]
  set httpUri [HTTP::uri]
  set httpRequestStart [clock clicks -milliseconds]
  set httpRequestTime [clock format [clock seconds] -format "%d/%h/%Y:%H:%M:%S %z"]
  set httpReferer [HTTP::header "Referer"]
  set httpUserAgent [HTTP::header "User-Agent"]

  # Dataretention headers
  # Check if header X-Forwarded-For exists
  if { [HTTP::header exists "X-Forwarded-For"] } {
    HTTP::header replace "X-Forwarded-For" "[HTTP::header "X-Forwarded-For"], $ipClient"
    HTTP::header replace "X-SMALS-Forwarded-For" "[HTTP::header "X-Forwarded-For"]"
  }
  if { !([HTTP::header exists "X-Forwarded-Host"]) } {
    HTTP::header insert "X-Forwarded-Host" "$httpHost"
  }
  if { !([HTTP::header exists "X-SMALS-Forwarded-For"]) } {
    HTTP::header insert "X-SMALS-Forwarded-For" "$ipClient"
  }
  if { !([HTTP::header exists "X-SMALS-Forwarded-Port"]) } {
    HTTP::header insert "X-SMALS-Forwarded-Port" "$ipLocalPort"
  }
  if { !([HTTP::header exists "X-SMALS-Forwarded-Host"]) } {
    HTTP::header insert "X-SMALS-Forwarded-Host" "$httpHost"
  }
  if { !([HTTP::header exists "X-Forwarded-For"]) } {
    HTTP::header insert "X-Forwarded-For" "$ipClient"
  }
  if { !([HTTP::header exists "X-Forwarded-Port"]) } {
    HTTP::header insert "X-Forwarded-Port" "$ipLocalPort"
  }
  if { $isClientSsl or $isPEP } {
    if { !([HTTP::header exists "X-Forwarded-Proto"]) } {
      HTTP::header insert "X-Forwarded-Proto" "https"
    }
    if { !([HTTP::header exists "X-SMALS-Forwarded-Proto"]) } {
      HTTP::header insert "X-SMALS-Forwarded-Proto" "https"
    }
    if { !([HTTP::header exists "IS_SSL"]) } {
      HTTP::header insert "IS_SSL" "ssl"
    }
    if { !([HTTP::header exists "X-Forwarded-Ssl"]) } {
      HTTP::header insert "X-Forwarded-Ssl" "on"
    }
    if { !([HTTP::header exists "X-Url-Scheme"]) } {
      HTTP::header insert "X-Url-Scheme" "https"
    }
    if { !([HTTP::header exists "Front-End-Https"]) } {
      HTTP::header insert "Front-End-Https" "on"
    }
  } else {
    if { !([HTTP::header exists "X-Forwarded-Proto"]) } {
      HTTP::header insert "X-Forwarded-Proto" "http"
    }
    if { !([HTTP::header exists "X-SMALS-Forwarded-Proto"]) } {
      HTTP::header insert "X-SMALS-Forwarded-Proto" "http"
    }
    if { !([HTTP::header exists "X-Url-Scheme"]) } {
      HTTP::header insert "X-Url-Scheme" "http"
    }
  }

  # Identify client Ip type
  HTTP::header insert Vary "X-Targetting"
  if { $smalsClientIp} {
    # Set the targetting audience and connection type
    HTTP::header insert "X-Targetting" "Smals"
    if { "$smalsClientIpType" ne "" } {
      HTTP::header insert "X-Targetting-Type" "$smalsClientIpType"
    }
  } else {
    HTTP::header insert "X-Targetting" "Other"
  }

  # Basic shielding of status pages
  switch -glob -- $httpPath \
    "/jolokia*" - \
    "/server-status*" {
      HTTP::respond 403
      call smalsProcs::accesslog_v1_403 "$ipClient" "$httpHost" "$httpRequestTime" "$httpMethod" "$httpUri" "$httpVersion" "$httpReferer" "$httpUserAgent" "$sslClientCipherNameLog" "$sslClientCipherVersionLog" "$sslClientCipherBitsLog" "$vsName" "$smalsEnv" "$partitionName"
      event disable
      return
    }

  # Block HTTP TRACE method
  if { "$httpMethod" equals "TRACE" } {
    HTTP::respond 405
    call smalsProcs::accesslog_v1_405 "$ipClient" "$httpHost" "$httpRequestTime" "$httpMethod" "$httpUri" "$httpVersion" "$httpReferer" "$httpUserAgent" "$sslClientCipherNameLog" "$sslClientCipherVersionLog" "$sslClientCipherBitsLog" "$vsName" "$smalsEnv" "$partitionName"
    event disable
    return
  }

  # Remove the Proxy header
  # Vulnerability: https://www.kb.cert.org/vuls/id/797896
  # Documentation: https://httpoxy.org/
  HTTP::header remove "Proxy"

  # Weblogic headers
  if { $isPEP == false} {
    HTTP::header remove "WL-Proxy-SSL"
    HTTP::header remove "WL-Proxy-Client-Cert"
    HTTP::header remove "WL-Proxy-Client-Keysize"
    HTTP::header remove "WL-Proxy-Client-Secretkeysize"
    HTTP::header remove "WL-Proxy-Client-IP"
    HTTP::header remove "X-WebLogic-KeepAliveSecs"
    HTTP::header remove "X-WebLogic-Request-ClusterInfo"
    HTTP::header remove "X-WebLogic-Cluster-Hash"

    HTTP::header insert "WL-Proxy-Client-IP" "[HTTP::header "X-Forwarded-For"]"
    HTTP::header insert "X-WebLogic-KeepAliveSecs" 30

    if { $isClientSsl } {
      HTTP::header insert "WL-Proxy-SSL" "true"
      HTTP::header insert "WL-Proxy-Client-Cert" $sslClientCert
      HTTP::header insert "WL-Proxy-Client-Keysize" $sslClientCipherBits
      HTTP::header insert "WL-Proxy-Client-Secretkeysize" $sslClientCipherBits
    }
  }
}

when HTTP_RESPONSE priority 5 {
  set httpStatus [HTTP::status]
}

when HTTP_RESPONSE_RELEASE priority 5 {
  # Triggered when the system parses all of the response status and header lines from the server response.


  if { [HTTP::header "access-network"] eq "INTERNET" } {
    HTTP::header remove "Via"
  }
  if { [HTTP::header "access-network"] eq "EXTRANET" } {
    HTTP::header remove "Via"
  }

  # Remove spurious HTTP headers
  HTTP::header remove "X-Powered-By"
  HTTP::header remove "X-Generator"
  HTTP::header remove "access-network"

  # Get Content-Length to show the response size in the logs
  # Does not work for Chunked Encoding since the Content-Length header is not available
  set httpContentLength [HTTP::header "Content-Length"]
  if { $httpContentLength eq "" } {
    set httpContentLength "0"
  }


  set httpContentType [HTTP::header "Content-Type"]
  if { $httpContentType eq "" } {
    set httpContentType "-"
  }

  # Calculate the response time
  set httpRequestDuration [expr { [clock clicks -milliseconds] - $httpRequestStart }]

  # Log request
  call  smalsProcs::accesslog_v1_general "$ipClient" "$httpHost" "$httpRequestTime" "$httpMethod" "$httpUri" "$httpVersion" "$httpReferer" "$httpUserAgent" "$sslClientCipherNameLog" "$sslClientCipherVersionLog" "$sslClientCipherBitsLog" "$vsName" "$smalsEnv" "$partitionName" "$httpRequestDuration" "$httpStatus" "$httpContentLength" "$httpContentType" "$lbPool" "$lbMember"
}

app-service none
partition Common
}
