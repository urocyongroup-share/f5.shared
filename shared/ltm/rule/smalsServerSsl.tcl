#ENV ALL
ltm rule smalsClientSsl {
# Managed by Git repo f5.shared
# Version: $Id$
# $Format:Git ID: (%h) %ci/%cn$

when CLIENTSSL_CLIENTHELLO priority 8 {
  # Triggered when the system has received the client's SSL ClientHello message

  # Set SSL traffic type marker
  set isClientSsl true

  # Initialize var
  set sslClientCert ""
}

when CLIENTSSL_CLIENTCERT priority 8 {
  # Triggered when the system adds an SSL client certificate to the client certificate chain.

  set sslClientCert [string map {"-----BEGIN CERTIFICATE-----" "" "-----END CERTIFICATE-----" "" "\n" ""} [X509::whole [SSL::cert 0]]]
  set sslClientCertStatusCode [SSL::verify_result]
  set sslClientCertStatusDesc [X509::verify_cert_error_string [SSL::verify_result]]
}

when CLIENTSSL_HANDSHAKE priority 8 {
  # Triggered when a client-side SSL handshake is completed.

  # Get SSL properties
  set sslClientCipherName [SSL::cipher name]
  set sslClientCipherVersion [SSL::cipher version]
  set sslClientCipherBits [SSL::cipher bits]
  set sslClientSessionId [SSL::sessionid]
  set sslClientSessionSecret [SSL::sessionsecret]
}

app-service none
partition Common
}
