#ENV ALL
ltm rule smalsEnv.xxx {
# Managed by Git repo f5.shared
# Version: $Id$
# $Format:Git ID: (%h) %ci/%cn$

when CLIENT_ACCEPTED priority 1 {
  # Triggered when a client has established a connection.
  
  set smalsEnv "xxx"

}

app-service none
partition Common
}
