#ENV
ltm rule smalsThrottling {
# Managed by Git (reponname)
# Version: $Id$
# $Format:Git ID: (%h) %ci/%cn$

when RULE_INIT {
  # This is the life timer of the subtable object
  # Defines how long this object exist in the subtable
  set static::maxRate 10

  # This defines how long is the sliding window to count the requests. 
  # This example allows 10 requests in 3 seconds
  set static::windowSecs 3
  set static::timeout 30
}

when HTTP_REQUEST {
  if { ["$httpMethod" eq "GET"] } {
    set getCount [table key -count -subtable $smalsClientIp]
    #log local0. "getCount=$getCount”
    if { $getCount < $static::maxRate } {
      incr getCount 1
      table set -subtable $smalsClientIp $getCount "ignore" $static::timeout $static::windowSecs
    } else {
      #log local0. "This user $user has exceeded the number of requests allowed."
      HTTP::respond 501 content "Request blockedExceeded requests/sec limit."
      return
    }
  }
}

app-service none
partition Common
}
