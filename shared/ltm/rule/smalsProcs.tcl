#ENV ALL
ltm rule smalsProcs {
# Managed by Git repo f5.shared
# Version: $Id$
# $Format:Git ID: (%h) %ci/%cn$

# iRule defining some general functions/procedures
# Doesn't need to be attached to a virtual server

# Partition specification seems to be required
# Observed with HSL::open

proc sendLog { hslPublisher hslMessage } {
  # Use catch to make sure a valid publisher was specified
  # Log localy if an error occurs
  if { [catch { set hslHandle [HSL::open -publisher /Common/$hslPublisher] }] } {
    log local0.info "Failed to open handle to hslPublisher $hslPublisher"
  } else {
    set hslReturn [HSL::send $hslHandle $hslMessage]
    if { $hslReturn != 1 } {
      log local0.info "HSL::send to $hslPublisher failed"
    }
  }
}

proc accesslog_v1_302 { ipClient httpHost httpRequestTime httpMethod httpUri httpVersion httpReferer httpUserAgent sslClientCipherNameLog sslClientCipherVersionLog sslClientCipherBitsLog vsName smalsEnv partitionName } {
  call smalsProcs::sendLog "lgsF5Acc" "v1 $ipClient $httpHost $httpRequestTime \"$httpMethod $httpUri HTTP/$httpVersion\" 0 302 0 - \"$httpReferer\" \"$httpUserAgent\" $sslClientCipherNameLog $sslClientCipherVersionLog $sslClientCipherBitsLog $vsName /Common/F5 127.0.0.1:0 $smalsEnv $partitionName\n"
}

proc accesslog_v1_403 { ipClient httpHost httpRequestTime httpMethod httpUri httpVersion httpReferer httpUserAgent sslClientCipherNameLog sslClientCipherVersionLog sslClientCipherBitsLog vsName smalsEnv partitionName } {
  call smalsProcs::sendLog "lgsF5Acc" "v1 $ipClient $httpHost $httpRequestTime \"$httpMethod $httpUri HTTP/$httpVersion\" 0 403 0 - \"$httpReferer\" \"$httpUserAgent\" $sslClientCipherNameLog $sslClientCipherVersionLog $sslClientCipherBitsLog $vsName /Common/F5 127.0.0.1:0 $smalsEnv $partitionName\n"
}

proc accesslog_v1_405 { ipClient httpHost httpRequestTime httpMethod httpUri httpVersion httpReferer httpUserAgent sslClientCipherNameLog sslClientCipherVersionLog sslClientCipherBitsLog vsName smalsEnv partitionName } {
  call smalsProcs::sendLog "lgsF5Acc" "v1 $ipClient $httpHost $httpRequestTime \"$httpMethod $httpUri HTTP/$httpVersion\" 0 405 0 - \"$httpReferer\" \"$httpUserAgent\" $sslClientCipherNameLog $sslClientCipherVersionLog $sslClientCipherBitsLog $vsName /Common/F5 127.0.0.1:0 $smalsEnv $partitionName\n"
}

proc accesslog_v1_general { ipClient httpHost httpRequestTime httpMethod httpUri httpVersion httpReferer httpUserAgent sslClientCipherNameLog sslClientCipherVersionLog sslClientCipherBitsLog vsName smalsEnv partitionName httpRequestDuration httpStatus httpContentLength httpContentType lbPool lbMember} {
  call smalsProcs::sendLog "lgsF5Acc" "v1 $ipClient $httpHost $httpRequestTime \"$httpMethod $httpUri HTTP/$httpVersion\" $httpRequestDuration $httpStatus $httpContentLength $httpContentType \"$httpReferer\" \"$httpUserAgent\" $sslClientCipherNameLog $sslClientCipherVersionLog $sslClientCipherBitsLog $vsName $lbPool $lbMember $smalsEnv $partitionName\n"
}

app-service none
partition Common
}
