#ENV ALL
ltm rule smalsDebug {
# Managed by Git (reponname)
# Version: $Id$
# $Format:Git ID: (%h) %ci/%cn$


when CLIENT_ACCEPTED priority 600 {
  set LogString "ipversion : $ipVersion | ipClient : $ipClient | ipLocalPort : $ipLocalPort | ipClientPort : $ipClientPort | lbSNAT : $lbSnat"
  log local0. "$LogString (CLIENT_ACCEPTED)"
}

when SERVER_CONNECTED priority 600 {
  set LogString "ipServer : $ipServer | ipServerPort : $ipServerPort"
  log local0. "$LogString (SERVER_CONNECTED)"
}

when LB_FAILED priority 600 {
  # Triggered when the system fails to select a pool or a pool member
  # or when a selected resource is unreachable.
  set LogString "$vsName $lbServer [event info]"
  log local0. "$LogString (LB_FAILED)"
}

when LB_SELECTED priority 600 {
  # Triggered when the system selects a pool member.
  set LogString "lbPool : $lbPool | lbMember : $lbMember"
  log local0. "$LogString (LB_SELECTED)"
}

when CLIENTSSL_HANDSHAKE priority 600 {
   set LogString "sslClientCipherName : $sslClientCipherName | sslClientCipherVersion: $sslClientCipherVersion | sslClientCipherBits: $sslClientCipherBits | TCP source port: $ipRemotePort | RSA Session-ID: $sslClientSessionId | Master-Key: $sslClientSessionSecret"
   log local0. "$LogString (CLIENTSSL_HANDSHAKE)"
}

when HTTP_REQUEST priority 600 {
   set LogString "$httpRequestTime Client $ipClient:$ipClientPort -> $httpMethod $httpVersion $httpHost$httpUri (virtualserver : $vsName)"
   log local0. "============================================="
   log local0. "$LogString (request)"
   foreach aHeader [HTTP::header names] {
      log local0. "$aHeader: [HTTP::header value $aHeader]"
   }
   log local0. "============================================="
}

when HTTP_RESPONSE priority 600 {
   log local0. "============================================="
   log local0. "$LogString (response) - status: $httpStatus"
   foreach aHeader [HTTP::header names] {
      log local0. "$aHeader: [HTTP::header value $aHeader]"
   }
   log local0. "============================================="   
}

when HTTP_RESPONSE_RELEASE priority 600 {
  set LogString "httpRequestDuration: $httpRequestDuration"
  log local0. "$LogString (HTTP_RESPONSE_RELEASE)"
}

}
