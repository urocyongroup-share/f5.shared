#ENV ALL
ltm rule smalsHttpAcl {

when HTTP_REQUEST priority 100 {

  # check if the client requested a protected host that has its own layer7 whitelist datagroup
  if { [class exists /$partitionName/acl_host_$httpHost] } {
    if { not [class match [IP::client_addr] equals /$partitionName/acl_host_$httpHost] } {
      set LogString "ipClient : $ipClient | ipLocalPort : $ipLocalPort | httpHost : $httpHost | httpPath : $httpPath | httpUri : $httpUri  | partitionName : $partitionName | aclfile : /$partitionName/acl_host_$httpHost"
      log local0. "$LogString (DROP LAYER 7 FW SMALS)"
      HTTP::respond 403
      call smalsProcs::accesslog_v1_403 "$ipClient" "$httpHost" "$httpRequestTime" "$httpMethod" "$httpUri" "$httpVersion" "$httpReferer" "$httpUserAgent" "$sslClientCipherNameLog" "$sslClientCipherVersionLog" "$sslClientCipherBitsLog" "$vsName" "$smalsEnv" "$partitionName"
      # Prevent processing of other iRules
      event disable all
      # Stop processing current event
      return
    }
  }

  # Check if the client requested a protected path
  switch -glob -- [HTTP::path] \
    "/administrator*" {
      switch -glob -- $httpHost \
        "portal.api.socialsecurity.be" - \
        "portal-prd.api.socialsecurity.be" - \
        "portal-acpt.api.socialsecurity.be" - \
        "portal-int.api.socialsecurity.be" - \
        "portal-sim.api.socialsecurity.be" - \
        "portalbf.api.ehealth.fgov.be" - \
        "portal.api.ehealth.fgov.be" - \
        "portalp1.api.ehealth.fgov.be" - \
        "portalp2.api.ehealth.fgov.be" - \
        "portalnr.api.ehealth.fgov.be" - \
        "portal-acpt.api.ehealth.fgov.be"
      {
          # Admin part needs to be accessible from everywhere
          set isProtectedPath true
        } \
        default {
          set isProtectedPath false
        }
    } \
    "/protectedpath*" {
      set isProtectedPath true
    } \
    default {
      set isProtectedPath false
    }

  # Check if the client requested a protected query
  switch -glob -- [HTTP::query] \
    "q=protectedquery" {
      set isProtectedQuery true
    } \
    default {
      set isProtectedQuery false
    }

  # Check if the client requested a protected host
  if { ($smalsEnv ne "prd") && ($smalsEnv ne "p1") && ($smalsEnv ne "p2") } {
    # Some exceptions NPRD
    switch -glob -- $httpHost \
      "reat.acc.pub.socialsecurity.be" - \
      "forms.mi-is.be" - \
      "portalbf.api.ehealth.fgov.be" - \
      "portal-acpt.api.ehealth.fgov.be" - \
      "portal-acpt.api.socialsecurity.be" - \
      "portal-sim.api.socialsecurity.be" - \
      "etee.int.pub.ehealth.fgov.be" - \
      "oauth2.acc.pub.socialsecurity.be" - \
      "exceptionNprdHost" {
        set isProtectedHost false
      } \
      default {
        # Deny all not explicitly defined hosts
        set isProtectedHost true
      }
  } else {
    # Some exceptions PRD
    switch -glob -- $httpHost \
      "analytics.api.ehealth.fgov.be" - \
      "analyticsnr.api.ehealth.fgov.be" - \
      "analyticsbf.api.ehealth.fgov.be" - \
      "analyticsp1.api.ehealth.fgov.be" - \
      "analyticsp2.api.ehealth.fgov.be" - \
      "analytics-acpt.api.ehealth.fgov.be" - \
      "analytics-intr2.api.ehealth.fgov.be" - \
      "analytics-intrc.api.ehealth.fgov.be" - \
      "analytics-int.api.ehealth.fgov.be" - \
      "analytics-test.api.ehealth.fgov.be" - \
      "admin.api.ehealth.fgov.be" - \
      "adminnr.api.ehealth.fgov.be" - \
      "adminbf.api.ehealth.fgov.be" - \
      "adminp1.api.ehealth.fgov.be" - \
      "adminp2.api.ehealth.fgov.be" - \
      "admin-acpt.api.ehealth.fgov.be" - \
      "admin-intr2.api.ehealth.fgov.be" - \
      "admin-intrc.api.ehealth.fgov.be" - \
      "admin-int.api.ehealth.fgov.be" - \
      "admin-test.api.ehealth.fgov.be" - \
      "preview.api.ehealth.fgov.be" - \
      "previewnr.api.ehealth.fgov.be" - \
      "previewbf.api.ehealth.fgov.be" - \
      "previewp1.api.ehealth.fgov.be" - \
      "previewp2.api.ehealth.fgov.be" - \
      "preview-acpt.api.ehealth.fgov.be" - \
      "preview-intr2.api.ehealth.fgov.be" - \
      "preview-intrc.api.ehealth.fgov.be" - \
      "preview-int.api.ehealth.fgov.be" - \
      "preview-test.api.ehealth.fgov.be" - \
      "git-cicd.gcloud.belgium.be" - \
      "exceptionPrdHost" - \
      "hfs-prd.socialsecurity.be" - \
      "hfs.socialsecurity.be" {
        set isProtectedHost true
      } \
      default {
        # Allow all not explicitly defined hosts
        set isProtectedHost false
      }
  }

  # Block access if the client requested a protected
  # path or host and is not in the allowed IP list
  if { ($isProtectedHost) || ($isProtectedPath) || ($isProtectedQuery) } {
    if { [class exists /$partitionName/smalsAcl] } {
      if { not [class match [IP::client_addr] equals /$partitionName/smalsAcl] } {
        set LogString "ipClient : $ipClient | ipLocalPort : $ipLocalPort | httpHost : $httpHost | httpPath : $httpPath | httpUri : $httpUri  | partitionName : $partitionName | aclfile : /$partitionName/smalsAcl"
        log local0. "$LogString (DROP LAYER 7 FW SMALS)"
        HTTP::respond 403
        call smalsProcs::accesslog_v1_403 "$ipClient" "$httpHost" "$httpRequestTime" "$httpMethod" "$httpUri" "$httpVersion" "$httpReferer" "$httpUserAgent" "$sslClientCipherNameLog" "$sslClientCipherVersionLog" "$sslClientCipherBitsLog" "$vsName" "$smalsEnv" "$partitionName"
        # Prevent processing of other iRules
        event disable all
        # Stop processing current event
        return
      }
    }
  }
}
}
