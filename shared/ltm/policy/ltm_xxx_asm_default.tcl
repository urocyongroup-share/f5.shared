#ENV
ltm policy /Common/ltm_xxx_asm_default {
    controls { asm }
    requires { http }
    rules {
        default {
            actions {
                0 {
                    asm
                    enable
                    policy /Common/_xxx_rapid_deployment_security_policy
                }
            }
            ordinal 1
        }
    }
    strategy /Common/first-match
}
