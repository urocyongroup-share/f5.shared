#ENV acc int
ltm node /Common/fluentdB1.xxx.services.gcloud.belgium.be {
    address _fluentdB1.xxx.services.gcloud.belgium.be_%3012
    app-service none
    connection-limit 0
    description "Managed by Git repo f5.shared - Version: : 6e5ab1ae45ec46ea516910db54ea35d614bd9644 $ - :Git ID: (%h) %ci - %cn(%ce)$"
    dynamic-ratio 1
    fqdn {
        address-family ipv4
        autopopulate disabled
        down-interval 5
        interval 3600
        name none
    }
    logging disabled
    metadata none
    monitor default
    partition Common
    rate-limit disabled
    ratio 1
    session monitor-enabled
    state up
}
