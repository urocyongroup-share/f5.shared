#ENV bcp 
ltm node /EH2/fluentdA1.xxx.ehealth2.be {
    address _fluentdA1.xxx.ehealth2.be_%31
    app-service none
    connection-limit 0
    description "Managed by Git repo f5.shared - Version: : 6e5ab1ae45ec46ea516910db54ea35d614bd9644 $ - :Git ID: (%h) %ci - %cn(%ce)$"
    dynamic-ratio 1
    fqdn {
        address-family ipv4
        autopopulate disabled
        down-interval 5
        interval 3600
        name none
    }
    logging disabled
    metadata none
    monitor default
    partition EH2
    rate-limit disabled
    ratio 1
    session monitor-enabled
    state up
}
