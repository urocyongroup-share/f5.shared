#ENV ALL
ltm profile one-connect onneconnectSmals {
    app-service none
    defaults-from oneconnect
    description "Managed by Git repo f5.shared - Version: : 6e5ab1ae45ec46ea516910db54ea35d614bd9644 $ - :Git ID: (%h) %ci - %cn(%ce)$"
    idle-timeout-override disabled
    limit-type none
    max-age 86400
    max-reuse 1000
    max-size 10000
    partition Common
    share-pools disabled
    source-mask 255.255.255.255
}
