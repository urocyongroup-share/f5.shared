#ENV ALL
ltm profile server-ssl smalsServerssl {
    alert-timeout indefinite
    allow-expired-crl disabled
    app-service none
    authenticate once
    authenticate-depth 9
    authenticate-name none
    ca-file smalsCABundle.crt
    cache-size 262144
    cache-timeout 3600
    cert none
    chain none
    ciphers DEFAULT
    crl-file none
    defaults-from none
    description "Managed by Git repo f5.shared - Version: : 6e5ab1ae45ec46ea516910db54ea35d614bd9644 $ - :Git ID: (%h) %ci - %cn(%ce)$"
    expire-cert-response-control drop
    generic-alert enabled
    handshake-timeout 10
    key none
    max-active-handshakes indefinite
    mod-ssl-methods disabled
    mode enabled
    options { dont-insert-empty-fragments }
    partition Common
    passphrase none
    peer-cert-mode require
    proxy-ssl disabled
    proxy-ssl-passthrough disabled
    renegotiate-period indefinite
    renegotiate-size indefinite
    renegotiation enabled
    retain-certificate true
    secure-renegotiation require-strict
    server-name none
    session-mirroring disabled
    session-ticket disabled
    sni-default false
    sni-require false
    ssl-forward-proxy disabled
    ssl-forward-proxy-bypass disabled
    ssl-sign-hash any
    strict-resume disabled
    unclean-shutdown enabled
    untrusted-cert-response-control drop
}
