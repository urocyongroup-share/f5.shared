#ENV
ltm profile ocsp-stapling-params QuoVadisOCSP {
    app-service none
    cache-error-timeout 3600
    cache-timeout indefinite
    clock-skew 300
    description "Managed by Git repo f5.shared - Version: : 6e5ab1ae45ec46ea516910db54ea35d614bd9644 $ - :Git ID: (%h) %ci - %cn(%ce)$"
    dns-resolver DNS
    partition Common
    proxy-server-pool none
    responder-url none
    sign-hash sha1
    signer-cert none
    signer-key none
    signer-key-passphrase none
    status-age 0
    strict-resp-cert-check disabled
    timeout 8
    trusted-ca GlobalSignOCSP.crt
    trusted-responders none
    use-proxy-server disabled
}
