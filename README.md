# F5 Control

This repo contains shared scripts and configuration for F5

## Usage

The checkout script will checkout one of the environment specific repos
The content of this repo will be placed in the ctl/${smalsProj}/bin directory

## Deploying configuration

Prereq: SSH trust to root@<f5>

```
./f5_checkout.sh release/R16.4 && cd / && cd -
./f5_config.sh
INFO:  Generated config for lbx
INFO:  Replaced environment variables and resolved DNS entries
INFO:  Copied config to lbx
INFO:  Copied system certificate to lbx
OK:    Reloaded httpd on lbx
OK:    Installed smalsCABundle.crt
OK:    Installed smalsSub1.crt
OK:    Installed prd.smals.be.crt
OK:    Installed prd.smals.be.key
Loading configuration...
  /tmp/lbx.tcl
OK:    Merged new config with running config on lbx
INFO:  Removed temporary files
INFO:  Waiting for devices to settle...
INFO:  Generated config for lbx
INFO:  Replaced environment variables and resolved DNS entries
INFO:  Copied config to lbx
INFO:  Copied system certificate to lbx
OK:    Reloaded httpd on lbx
OK:    Installed smalsCABundle.crt
OK:    Installed smalsSub1.crt
OK:    Installed prd.smals.be.crt
OK:    Installed prd.smals.be.key
Loading configuration...
  /tmp/lbx.tcl
OK:    Merged new config with running config on lbx
INFO:  Removed temporary files
INFO:  Waiting for devices to settle...
```

## Running a manual back-up

```
./f5_backupwrapper.sh
INFO:  /<backup location>/f5xxx/data created
INFO:  Backup script copied to lbx
INFO:  Restarted REST daemon on lbx (https://support.f5.com/kb/en-us/solutions/public/16000/700/sol16751.html)
OK:    Backup script executed on lbx
Gathering System Diagnostics: Please wait ...
Diagnostic information has been saved in:
/var/tmp/lbx-20170112-094737.qkview
Please send this file to F5 support.
OK:    Generated qkview for lbx
OK:    Successfully transferred files to /<backup location>/f5xxx/data
INFO:  Cleaned old files in /<backup location>/f5xxx/data
INFO:  /<backup location>/f5xxx/data created
INFO:  Backup script copied to lbx
INFO:  Restarted REST daemon on lbx (https://support.f5.com/kb/en-us/solutions/public/16000/700/sol16751.html)
OK:    Backup script executed on lbx
Gathering System Diagnostics: Please wait ...
Diagnostic information has been saved in:
/var/tmp/lbx-20170112-095603.qkview
Please send this file to F5 support.
OK:    Generated qkview for lbx
OK:    Successfully transferred files to /<backup location>/f5xxx/data
INFO:  Cleaned old files in /<backup location>/f5xxx/data
```
