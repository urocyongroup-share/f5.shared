#!/bin/ksh

##### CONFIG @@@@@
GITLAB_SERVER="gitlab.gcloud.belgium.be"
GITLAB_API_VERSION="v4"
GITLAB_TOKEN="ayyyyQ_8zm8AEdeAG_kb"
##################

echoError () {
  echo -e "\033[1;31m""ERROR:  ${1}""\033[0m"
}

echoWarn () {
  echo -e "\033[0;33m""WARN:   ${1}""\033[0m"
}

echoInfo () {
  echo "INFO:   ${1}"
}

echoOk () {
  echo -e "\033[0;32m""OK:     ${1}""\033[0m"
}

echoOkMeta () {
  echo -e "\033[0;32m""${1}""\033[0m"
}

# Read secret string
read_secret()
{
  echo $1
  # Disable echo.
  stty -echo

  # Set up trap to ensure echo is enabled before exiting if the script
  # is terminated while echo is disabled.
  trap 'stty echo' EXIT

  # Read secret.
  read "$2"

  # Enable echo.
  stty echo
  trap - EXIT

  # Print a newline because the newline entered by the user after
  # entering the passcode is not echoed. This ensures that the
  # next line of output begins at a new line.
  echo
}

ShowUsage () {
  echo -e "\nUSAGE: $0 -r <reference> -e <environment>";
}

Execute () {
  selectproject
  deletetag
  createtag
}

selectproject () {
  smalsProjDir=$(cd ../..;pwd;cd -)
  smalsProj=$(basename $smalsProjDir)
  REPO="$smalsProj"
  echoInfo "get project id for repo ${REPO}"
  HTTP_BODY=""
  HTTP_RETURN=$(curl -s -k -H "Private-Token:  ${GITLAB_TOKEN}" -X GET "https://${GITLAB_SERVER}/api/${GITLAB_API_VERSION}/projects?search=${REPO}")
  RC=$?
  if [[ ( ${RC} -eq 0 ) && ( "${HTTP_RETURN}" !=  *"errors"* )    ]]
  then
    echoOk "Get project id for ${REPO} succesful!"
    #echoOk "${HTTP_RETURN}"
    F5_PROJECT=$(echo "${HTTP_RETURN}" | jq -r ".[].id")
  else
    echoError "Issue occured while getting project id for repo ${REPO}"
    echoError "${HTTP_RETURN}"
    echo ${OUTPUT}
    exit 1
  fi
}

deletetag () {
  echoInfo "delete current tag ${ENV}"
  HTTP_BODY=""
  HTTP_RETURN=$(curl -s -k -H "Private-Token:  ${GITLAB_TOKEN}" -X DELETE "https://${GITLAB_SERVER}/api/${GITLAB_API_VERSION}/projects/${F5_PROJECT}/repository/tags/${ENV}")
  RC=$?
  if [[ ( ${RC} -eq 0 ) && ( "${HTTP_RETURN}" !=  *"errors"* )    ]]
  then
    echoOk "Delete current tag ${ENV} succesful!"
    #echoOk "${HTTP_RETURN}"
  else
    echoError "Issue occured while removing current tag ${ENV}"
    echoError "${HTTP_RETURN}"
    echo ${OUTPUT}
    exit 1
  fi
}

createtag () {
  echoInfo "create tag ${ENV}"
  HTTP_BODY=""
  HTTP_RETURN=$(curl -s -k -H "Private-Token:  ${GITLAB_TOKEN}" -X POST "https://${GITLAB_SERVER}/api/${GITLAB_API_VERSION}/projects/${F5_PROJECT}/repository/tags?tag_name=${ENV}&ref=${REF}")
  RC=$?
  if [[ ( ${RC} -eq 0 ) && ( "${HTTP_RETURN}" !=  *"errors"* )    ]]
  then
    echoOk "Creation tag ${ENV} with reference ${REF} was succesful!"
    #echoOk "${HTTP_RETURN}"
    commit_author_name=$(echo "${HTTP_RETURN}" | jq -r ".commit.author_name")
    commit_author_email=$(echo "${HTTP_RETURN}" | jq -r ".commit.author_email")
    commit_message=$(echo "${HTTP_RETURN}" | jq -r ".commit.message")
    commit_date=$(echo "${HTTP_RETURN}" | jq -r ".commit.committed_date")
    echoOkMeta "\n\t====================================\n\tCommit author:\t${commit_author_name} <${commit_author_email}> \n\tCommit date:\t${commit_date} \n\tCommit message:\t${commit_message}\n\t====================================\n"
  else
    echoError "Issue occured while creating tag ${ENV} with reference ${REF}"
    echoError "${HTTP_RETURN}"
    echo ${OUTPUT}
    exit 1
  fi
}


OPTION_R=0; OPTION_E=0

while getopts "r:e:" option; do
  case ${option} in
    "r" )
      export REF=${OPTARG}
      OPTION_R=1
    ;;
    "e" )
      export ENV=${OPTARG}
      OPTION_E=1
    ;;
    \?      )
      ShowUsage
      return 1
    ;;
  esac
done


if [[ ${OPTION_R} -eq 0 ]]; then
  echoError "command was ABORTED - Please use the \"-r <reference>\" option to secify the the reference"
  ShowUsage
  exit 1
fi
if [[ ${OPTION_E} -eq 0 ]]; then
  echoError "command was ABORTED - Please use the \"-e <env>\" option to secify the the environment/tage"
  ShowUsage
  exit 1
fi

Execute
