
resolvesecret () {
  localFile=$1
  for SECRET in $( grep -o '\[vault\]\[[^ ]*\]' ${localFile}); do
    uri=$(echo ${SECRET} | cut -d '[' -f3 | tr -d ']')
    field=$(echo ${SECRET} | cut -d '[' -f4 | tr -d ']')
    secret=$(curl -s -k --header "X-Vault-Token: ${VAULT_TOKEN}" "${VAULT_ENDPOINT}/${VAULT_APIVERSION}/${VAULT_SECRETBACKEND}/${uri}" | jq .data.${field} | tr -d '"' | sed "s/[!@#=+{}[~;:,<>/?'$%^&*()-]/\\\&/g")
    escapedSECRET=$(echo ${SECRET} | sed 's/\//\\\//g' | sed 's/\[/\\\[/g' | sed 's/\]/\\\]/g')
    sed -i "s/${escapedSECRET}/${secret}/g" ${localFile}
  done
}

# get vault config parameters
baseDir=$(dirname $(readlink -f "$0"))
. ${baseDir}/.vaultconfig

# execute resolve secret
resolvesecret $1
