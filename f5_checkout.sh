#!/bin/ksh

echoError () {
    echo -e "\033[1;31m""ERROR: ${1}""\033[0m"
}

echoWarn () {
    echo -e "\033[0;33m""WARN: ${1}""\033[0m"
}

echoInfo () {
    echo "INFO: ${1}"
}

echoOk () {
    echo -e "\033[0;32m""OK:   ${1}""\033[0m"
}

CheckGitConnection () {
    OUTPUT=$( git ls-remote ${GITLAB_REPO_URL} HEAD )
    RC=$?

    if [[ ${RC} -eq 0 ]]
    then
        echoInfo "Git connection to ${GITLAB_REPO_URL} succesful!"
    else
        echoError "Issue occured while connecting to ${GITLAB_REPO_URL}"
        echo ${OUTPUT}
        exit 1
    fi

}

CheckBranchName () {

    GIT_BRANCHES=$( git ls-remote ${GITLAB_REPO_URL} | grep -oP "(?<=heads\/)(?!master)\S*|(?<=tags\/)(?!master)\S*|HEAD" | sort )

    if [[ -z $( echo ${GIT_BRANCHES} | grep -iowP "${RELEASE}" ) ]]
    then
         echoError "Branch name \"${RELEASE}\" is not valid!"
         echo "Please choose one of the following: "
         for BRANCH in ${GIT_BRANCHES}
         do
             echo "  * ${BRANCH}"
         done
         exit 1
    fi
}

RefreshFolder () {

    THIRD_LEVEL=$1
    #DIR=${smalsLibRoot}/${THIRD_LEVEL}/${smalsProj}
    DIR="$(dirname $(dirname $(pwd)))"

    echoInfo "Refreshing directory ${DIR}"

    # Check if the specified directory exists
    if [[ ! -d ${DIR} ]]
    then
        echoWarn "Directory ${DIR} does not exist. Skipping..."
        return 1
    fi

    # Remove the old backup & branch file
    rm -f ${DIR}/.branch
    if [[ -d ${DIR}/.old ]]
    then
        rm -rf ${DIR}/.old
    fi

    # If DIR is not empty, create new backup
    if [[ $( ls ${DIR}/* ) ]]
    then
        echoInfo "Creating backup of ${DIR}/* to ${DIR}/.old"
        mkdir ${DIR}/.old
        mv ${DIR}/ctl ${DIR}/.old
        if [[ $? -ne 0 ]]
        then
            echoWarn "Something went wrong while creating the backup. Refresh for the directory: ${DIR} will not be performed."
            return 1
        fi
        mkdir -p  ${DIR}/ctl/bin
    fi

    # Get files from gitlab
    echoInfo "Retreiving files for ${THIRD_LEVEL} from gitlab..."
    git archive --format=tar --remote=${GITLAB_REPO_URL} ${RELEASE} ${THIRD_LEVEL} | tar --overwrite --directory=${DIR}/${THIRD_LEVEL} --strip-components=1 -xf -

    if [[ $? -eq 0 ]]
    then
        echoOk "Succesfully refreshed ${THIRD_LEVEL}!"
        # Create file with info about current branch
        echo ${RELEASE} > ${DIR}/.branch
    else
        echoError "Something went wrong with the git command!"
        return 1
    fi

    return 0
}

RefreshSubmodule () {

    SUBMODULE=$1
    #DIR=${smalsLibRoot}/ctl/${smalsProj}/${SUBMODULE}/
    DIR="$(pwd)"
    echoInfo "Refreshing directory ${DIR}"

    # Check if the specified directory exists
    if [[ ! -d ${DIR} ]]
    then
        echoWarn "Directory ${DIR} does not exist. Skipping..."
        return 1
    fi

    # Get files from gitlab
    echoInfo "Retreiving files for ${SUBMODULE} from gitlab..."
    git archive --format=tar --remote=${GITLAB_REPO_URL} ${RELEASE} 2> /dev/null | tar --overwrite --directory=${DIR} -xf - 2> /dev/null || git archive --format=tar --remote=${GITLAB_REPO_URL} HEAD | tar --overwrite --directory=${DIR} -xf -

    if [[ $? -eq 0 ]]
    then
        echoOk "Succesfully refreshed ${SUBMODULE}!"
        # Create file with info about current branch
        echo ${RELEASE} > ${DIR}/.branch
    else
        echoError "Something went wrong with the git command!"
        return 1
    fi

    return 0
}

#####################################################################
#
#   MAIN
#
#####################################################################

# Set environment
#. /start/lib/rts/common/bin/set_env.sh
smalsProjDir=$(cd ../..;pwd;cd -)
smalsProj=$(basename $smalsProjDir)
# Get branch/release name
if [[ -z $1 ]];then
        echoError "Please provide branch name to checkout. ex : \"$0 R16.1\""
        exit 1
else
        RELEASE=${1}
fi

# Set gitlab vars
GITLAB_USER=git
GITLAB_HOST=gitlab.com
GITLAB_SSH_PORT=22
GITLAB_GROUP=urocyongroup-share

GITLAB_REPO=${smalsProj}
GITLAB_REPO_URL=${GITLAB_USER}@${GITLAB_HOST}:${GITLAB_GROUP}/${GITLAB_REPO}

# First check if the connection to the gitlab server is available:
CheckGitConnection

# Next, check if the provided branch name exists
CheckBranchName

# Refresh with git:
RefreshFolder ctl

# Refresh submodule
GITLAB_REPO=f5.shared
RELEASE="master"
GITLAB_REPO_URL=${GITLAB_USER}@${GITLAB_HOST}:${GITLAB_GROUP}/${GITLAB_REPO}

# Refresh with git:
RefreshSubmodule bin

# Renew directory
echoInfo "Current directory becomes unvalid. Run following command to go to real ctl dir: \"cd / && cd -\"."

exit 0
