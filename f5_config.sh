#!/bin/ksh

ShowUsage () {

    ${ECHO} -e "\tUSAGE: $0 [-h <HostName> ] -e <Environment>";
    cat << EndOfUsage


                Options
                    -h <HostName>
                    -e <environment> MANDATORY option to prevent deploying in wrong environment
                    -f fast deployment (scipping certificate and asm policy upload)
                    -s deploy to one node of the sync-cluster
                    -v verify the config without changing it
                    -r restore config [ latest | <date> ]

EndOfUsage
}

echoError () {
    echo -e "\033[1;31m""ERROR: ${1}""\033[0m"
}

echoWarn () {
    echo -e "\033[0;33m""WARN:  ${1}""\033[0m"
}

echoInfo () {
    echo "INFO:  ${1}"
}

echoOk () {
    echo -e "\033[0;32m""OK:    ${1}""\033[0m"
}

bacupSCF () {
  echoInfo "Taking scf backup before deploy"
  ssh ${sshOpts} middleware@${f5Host} "tmsh save /sys config file ${remoteTmpSCF}.tcl no-passphrase tar-file ${remoteTmpSCF}.tar"
  rc=$?
  if [[ $rc -ne 0 ]]; then
    echoError "Backup running config to scf backup file ${remoteTmpSCF}.tcl and ${remoteTmpSCF}.tar failed"
    exit $rc
  else
    echoOk "Backup running config to scf backup file ${remoteTmpSCF}.tcl and ${remoteTmpSCF}.tar succesful"
  fi
  scp ${sshOpts} middleware@${f5Host}:${remoteTmpSCF}* ${localBackupDir}
  rc=$?
  if [[ $rc -ne 0 ]]; then
    echoError "Copying remote ${remoteTmpSCF} from ${f5Host} to local ${localBackupDir} failed"
    exit $rc
  else
    echoOk "Copied backup remote ${remoteTmpSCF} from ${f5Host} to local ${localBackupDir} succesful"
  fi
  unlink ${localBackupDir}/${f5Host}-scf-latest.tcl
  ln -s $(basename ${remoteTmpSCF}).tcl ${localBackupDir}/${f5Host}-scf-latest.tcl
  unlink ${localBackupDir}/${f5Host}-scf-latest.tar
  ln -s $(basename ${remoteTmpSCF}).tar ${localBackupDir}/${f5Host}-scf-latest.tar
  echoInfo "Cleanup backup on F5"
  ssh ${sshOpts} middleware@${f5Host} "rm -rf ${remoteTmpSCF}.tcl ${remoteTmpSCF}.tar"
  rc=$?
  if [[ $rc -ne 0 ]]; then
    echoError "Cleanup ${remoteTmpSCF}.tcl ${remoteTmpSCF}.tar failed"
    exit $rc
  else
    echoOk "Cleanup ${remoteTmpSCF}.tcl ${remoteTmpSCF}.tar succesful"
  fi

}

restoreSCF () {
  echoInfo "Trying to restore backup to ${f5Host}"
  scp ${sshOpts} ${localBackupDir}/${f5Host}-scf-${restoreDate}* middleware@${f5Host}:${remoteRestoreSCF}
  rc=$?
  if [[ $rc -ne 0 ]]; then
    echoError "SCP backup ${f5Host}-scf-${restoreDate} to ${f5Host} failed"
    exit $rc
  else
    echoOk "SCP backup ${f5Host}-scf-${restoreDate} to ${f5Host} succesful"
  fi
  ssh ${sshOpts} middleware@${f5Host} "tmsh load /sys config file ${remoteRestoreSCF}/${f5Host}-scf-${restoreDate}.tcl  tar-file ${remoteRestoreSCF}/${f5Host}-scf-${restoreDate}.tar"
  rc=$?
  if [[ $rc -ne 0 ]]; then
    echoError "Restore backup ${f5Host}-scf-${restoreDate} on ${f5Host} failed"
    exit $rc
  else
    echoOk "Restored backup ${f5Host}-scf-${restoreDate} on ${f5Host} succesful"
  fi
  echoInfo "Cleanup backup on F5"
  ssh ${sshOpts} middleware@${f5Host} "rm -rf ${remoteRestoreSCF}/${f5Host}-scf-${restoreDate}.tcl ${remoteRestoreSCF}/${f5Host}-scf-${restoreDate}.tar"
  rc=$?
  if [[ $rc -ne 0 ]]; then
    echoError "Cleanup ${remoteRestoreSCF}/${f5Host}-scf-${restoreDate}.tcl ${remoteRestoreSCF}/${f5Host}-scf-${restoreDate}.tar"
    exit $rc
  else
    echoOk "Cleanup ${remoteRestoreSCF}/${f5Host}-scf-${restoreDate}.tcl ${remoteRestoreSCF}/${f5Host}-scf-${restoreDate}.tar"
  fi

}


statusVault () {
  HTTP_RETURN=$(curl -s -k "${VAULT_ENDPOINT}/${VAULT_APIVERSION}/sys/health")
  RC=$?
  if [[ ( ${RC} -eq 0 ) && ( "${HTTP_RETURN}" !=  *"errors"* ) && ( "$(echo "${HTTP_RETURN}" | jq .sealed)" != "true" )  ]]
  then
    echoInfo "Vault is available and not sealed"
    echoOk "${HTTP_RETURN}"
  else
    echoError "Issue occured while executing vault check to ${VAULT_ENDPOINT}$"
    echoError "${HTTP_RETURN}"
    echo ${OUTPUT}
    exit 1
  fi
}

generateConfig () {

  > ${localTmp}
  #for config in $(find ${baseDir}/shared ${baseDir}/../parm -name '*.tcl' 2> /dev/null); do
  # config merge failes when not specifying the explicit order
  for config in $(find \
    ${baseDir}/shared/net \
    ${baseDir}/shared/auth \
    ${baseDir}/shared/sys \
    ${baseDir}/shared/ltm \
    ${baseDir}/shared/cli \
    ${baseDir}/shared/instance \
    ${baseDir}/../parm/net \
    ${baseDir}/../parm/instance \
    ${baseDir}/../parm/cm \
    ${baseDir}/../parm/ltm \
    ${baseDir}/../parm/asm \
    ${baseDir}/../parm/sys \
    -name '*.tcl' 2> /dev/null); do
    if [[ $(egrep -i "^#ENV .*${zone}${env}${node}.*|^#ENV .*${zone}${env}( .*|$)|^#ENV .*${zone}( .*|$)|^#ENV .*${env}( .*|$)|^#ENV ALL" ${config}) != "" ]]; then
      cat ${config} >> ${localTmp}
    fi
  done
  echoInfo "Generated config for ${f5Host}"

}

parseConfig () {

  sed -i "s/xxx/${env}/g" ${localTmp}

  # apply key-propertie file config
  if [ -f ${baseDir}/../parm/instance/${env}.kps ]; then
    while IFS=';' read -r key value; do
      sed -i "s/_${key}_/${value}/g" ${localTmp}
    done < ${baseDir}/../parm/instance/${env}.kps
  fi

  # Gather hostnames
  set -A aFQDN
  for FQDN in $(grep -o '_[^ ]*_' ${localTmp}); do
    FQDN=$(echo ${FQDN} | awk -F'_' '{print $2}')
    # Check if we haven an actual FQDN
    if [[ ${FQDN} == *"."* ]]; then
      # Make sure we don't add duplicates to the array
      if [[ ${aFQDN[@]} != *"${FQDN}"* ]]; then
        aFQDN[${#aFQDN[*]}+1]=${FQDN}
      fi
    fi
  done

  # Resolve hostnames
  for FQDN in ${aFQDN[@]}; do
    ip=$(getent hosts ${FQDN} | awk '{ print $1}' | sort -n | head -n 1)
    if [[ ${ip} != "" ]]; then
      sed -i "s/_${FQDN}_/${ip}/g" ${localTmp}
    else
      ip=$(host ${FQDN} | grep "has address" | awk -F' ' '{print $4}' | sort -n | head -n 1)
      if [[ ${ip} != "" ]]; then
        sed -i "s/_${FQDN}_/${ip}/g" ${localTmp}
      else
        ip=$(host ${FQDN} | grep "has IPv6 address" | awk -F' ' '{print $5}' | sort -n | head -n 1)
        if [[ ${ip} != "" ]]; then
          sed -i "s/_${FQDN}_/${ip}/g" ${localTmp}
        else
            echoError "No ip found for ${FQDN}"
            exit 1
        fi
      fi
    fi
  done

  for SECRET in $( grep -o '\[vault\]\[[^ ]*\]' ${localTmp}); do
    uri=$(echo ${SECRET} | cut -d '[' -f3 | tr -d ']')
    field=$(echo ${SECRET} | cut -d '[' -f4 | tr -d ']')
    secret=$(curl -s -k --header "X-Vault-Token: ${VAULT_TOKEN}" "${VAULT_ENDPOINT}/${VAULT_APIVERSION}/${VAULT_SECRETBACKEND}/${uri}" | jq .data.${field} | tr -d '"')
    RC=$?
    if [[ ( ${RC} -eq 0 ) && ( "${HTTP_RETURN}" !=  *"errors"* )    ]]
    then
      escapedSECRET=$(echo ${SECRET} | sed 's/\//\\\//g' | sed 's/\[/\\\[/g' | sed 's/\]/\\\]/g')
      sed -i "s/${escapedSECRET}/${secret}/g" ${localTmp}
      echoInfo "Replace secrets : ${uri} - ${field}"
    else
      echoError "Issue occured while replaced environment variables from VAULT for secret ${uri} - ${field}"
      echoError "${HTTP_RETURN}"
      echo ${OUTPUT}
      exit 1
    fi
  done

}

copyConfigLb () {

  scp ${sshOpts} ${localTmp} middleware@${f5Host}:${remoteTmp}
  rc=$?
  if [[ $rc -ne 0 ]]; then
    echoError "Copying ${localTmp} to ${f5Host} failed"
    exit $rc
  else
    echoInfo "Copied config to ${f5Host}"
  fi

}


installAsmPolicy () {

  for localFile in $(find ${baseDir}/shared/asm ${baseDir}/../parm/asm -name '*.xml' 2> /dev/null); do
    file=$(basename ${localFile} | sed "s/xxx/${env}/g")
    case ${file} in
      *.xml) filenoextention=$(basename ${file} .xml ) ;;
      *.bin) filenoextention=$(basename ${file} .bin) ;;
      *) echoError "Install of ASM ${file} failed" ;;
    esac
    scp ${sshOpts} ${localFile} middleware@${f5Host}:/var/tmp/${file} && \
    echoInfo "Copied ASM policy ${filenoextention} to ${f5Host}"
    ssh ${sshOpts} middleware@${f5Host} "tmsh load asm policy ${filenoextention} overwrite file /var/tmp/${file}"
    echoInfo "Loaded ASM policy ${filenoextention} on ${f5Host}"
    ssh ${sshOpts} middleware@${f5Host} "tmsh modify asm policy ${filenoextention} active"
    ssh ${sshOpts} middleware@${f5Host} "tmsh publish asm policy ${filenoextention}"
    echoInfo "Published ASM policy ${filenoextention} on ${f5Host}"
    rc=$?
    if [[ $rc -ne 0 ]]; then
      echoError "Install of ASM ${file} failed"
      exit $rc
    else
      echoOk "Installed ASM ${file}"
    fi
  done

}

installSysCert () {

  scp ${sshOpts} ${baseDir}/../parm/ssl/${f5Host}.crt middleware@${f5Host}:/etc/httpd/conf/ssl.crt/server.crt && \
  scp ${sshOpts} ${baseDir}/shared/ssl/smalsSub1.crt middleware@${f5Host}:/etc/httpd/conf/ssl.crt/server.chain && \
  scp ${sshOpts} ${baseDir}/../parm/ssl/${f5Host}.key middleware@${f5Host}:/etc/httpd/conf/ssl.key/server.key

  if [[ $rc -ne 0 ]]; then
    echoError "Failed to copy system certificate to ${f5Host}"
  else
    echoInfo "Copied system certificate to ${f5Host}"
  fi
  # Reload httpd
  ssh ${sshOpts} middleware@${f5Host} "service httpd reload"  > /dev/null
  rc=$?
  if [[ $rc -ne 0 ]]; then
    echoError "httpd reload for ${f5Host} failed"
    exit $rc
  else
    echoOk "Reloaded httpd on ${f5Host}"
  fi

}

installCert () {

  scp ${sshOpts} .vaultconfig middleware@${f5Host}:/var/tmp/.vaultconfig

  for localFile in $(find ${baseDir}/shared ${baseDir}/../parm/ssl -name '*.crt' 2> /dev/null); do
    if [[ $(egrep -i "^#ENV .*${zone}${env}${node}.*|^#ENV .*${zone}${env}( .*|$)|^#ENV .*${zone}( .*|$)|^#ENV .*${env}( .*|$)|^#ENV ALL" ${localFile}) != "" ]]; then
      file=$(basename ${localFile})
      # execute this for now local till flux is open
      ./f5_getsecret.sh ${localFile}
      scp ${sshOpts} f5_getsecret.sh middleware@${f5Host}:/var/tmp/f5_getsecret.sh && \
      scp ${sshOpts} ${localFile} middleware@${f5Host}:/var/tmp/${file} && \
      #ssh ${sshOpts} middleware@${f5Host} "/var/tmp/f5_getsecret.sh /var/tmp/${file}" && \
      ssh ${sshOpts} middleware@${f5Host} "tmsh install sys crypto cert ${file} from-local-file /var/tmp/${file}"
      rc=$?
      if [[ $rc -ne 0 ]]; then
        echoError "Install of ${file} failed"
        exit $rc
      else
        echoOk "Installed ${file}"
      fi
    fi
  done

  for localFile in $(find ${baseDir}/shared ${baseDir}/../parm/ssl -name '*.key' 2> /dev/null); do
    if [[ $(egrep -i "^#ENV .*${zone}${env}${node}.*|^#ENV .*${zone}${env}( .*|$)|^#ENV .*${zone}( .*|$)|^#ENV .*${env}( .*|$)|^#ENV ALL" ${localFile}) != "" ]]; then
      file=$(basename ${localFile})
      # execute this for now local till flux is open
      ./f5_getsecret.sh ${localFile}
      scp ${sshOpts} f5_getsecret.sh middleware@${f5Host}:/var/tmp/f5_getsecret.sh && \
      scp ${sshOpts} ${localFile} middleware@${f5Host}:/var/tmp/${file} && \
      #ssh ${sshOpts} middleware@${f5Host} "/var/tmp/f5_getsecret.sh /var/tmp/${file}" && \
      ssh ${sshOpts} middleware@${f5Host} "tmsh install sys crypto key ${file} from-local-file /var/tmp/${file}"
      rc=$?
      if [[ $rc -ne 0 ]]; then
        echoError "Install of ${file} failed"
        exit $rc
      else
        echoOk "Installed ${file}"
      fi
    fi
  done

}

installConfig () {

  ssh ${sshOpts} middleware@${f5Host} "tmsh load sys config merge file ${remoteTmp} && tmsh save sys config"
  rc=$?
  if [[ $rc -ne 0 ]]; then
    echoError "Config merge for ${f5Host} failed"
    exit $rc
  else
    echoOk "Merged new config with running config on ${f5Host}"
  fi

}

verifyConfig () {

  ssh ${sshOpts} middleware@${f5Host} "tmsh load sys config verify merge file ${remoteTmp}"
  rc=$?
  if [[ $rc -ne 0 ]]; then
    echoError "Config verify for ${f5Host} failed"
    exit $rc
  else
    echoOk "Verified new config with running config on ${f5Host}"
  fi

}

generatePacketFilter () {

  # Create base rules
  > ${localTmp}
  cat ${pfInput} | while IFS=";" read action appservice bwcpolicy logging rateclass rule vlan; do
  # Ignore comments
  if [[ "${action}" != \#* ]]; then
    # Generate high level syntax
    echo "net packet-filter x { action ${action} app-service ${appservice} bwc-policy ${bwcpolicy} description ${rule//.prm/} logging ${logging} order x rate-class ${rateclass} rule ${rule} vlan ${vlan} }" >> ${localTmp}
  fi
done

  # Resolve object names
  while [[ $(grep -c -o "[^ ]*.prm" ${localTmp}) != 0 ]]; do
    for objectName in $(grep -o "[^ ]*.prm" ${localTmp}); do
      objectDefinition=$(cat ${baseDir}/../parm/net/packet-filter/objects/${objectName})
      if [[ ${objectDefinition} == "" ]]; then
        echoError "Empty object ${objectName}"
        exit 1
      else
        sed -i "s|${objectName}|${objectDefinition}|g" ${localTmp}
      fi
    done
  done

  parseConfig

  # Fix src / dst group syntax
  grep -ho " src[^)]*" ${localTmp} | while read line; do
    fixedLine=$(echo ${line} | sed 's/ net/ src net/g; s/ host/ src host/g; s/src src/src/g')
    sed -i "s|${line}|${fixedLine}|g" ${localTmp}
  done
  grep -ho " dst[^)]*" ${localTmp} | while read line; do
    fixedLine=$(echo ${line} | sed 's/ net/ dst net/g; s/ host/ dst host/g; s/dst dst/dst/g')
    sed -i "s|${line}|${fixedLine}|g" ${localTmp}
  done

  # Generate rule name and priority
  index=1
  content=$(cat ${localTmp})
  > ${localTmp}
  echo "${content}" | while read line; do
    echo ${line} | sed "s/net packet-filter x/R${index}/g ; s/order x/order ${index}/g" >> ${localTmp}
    index=$((index+1))
  done

}

updatePacketFilter () {

  copyConfigLb
  # Deploy ruleset
  ssh ${sshOpts} middleware@${f5Host} "tmsh run cli script net.packet-filter.tcl ${remoteTmp} && tmsh save sys config"
  rc=$?
  if [[ $rc -ne 0 ]]; then
    echoError "Updating packet-filter rules on ${f5Host} failed"
    exit $rc
  else
    echoOk "Updated packet-filter rules on ${f5Host}"
  fi

}

cleanUp () {

  \rm -f ${localTmp}
  ssh ${sshOpts} middleware@${f5Host} "rm -f ${remoteTmp} /var/tmp/*.crt /var/tmp/*.key"
  rc=$?
  if [[ $rc -ne 0 ]]; then
    echoError "Failed to remove ${f5Host}:${remoteTmp}"
    exit $rc
  else
    echoInfo "Removed temporary files"
  fi

}


#. /start/lib/rts/common/bin/set_env.sh
set -o pipefail
sshOpts="-o StrictHostKeyChecking=no -o CheckHostIP=no -o PasswordAuthentication=no -o Protocol=2 -q"
baseDir=$(dirname $(readlink -f "$0"))
DATETIME="`date +%Y%m%d-%H%M%S`"
# read config
. ${baseDir}/.vaultconfig

# default actions are all zero
OPTION_E=0; OPTION_H=0; OPTION_F=0; OPTION_S=0; OPTION_V=0; OPTION_R=0;

while getopts "fvsr:h:e:" option
do
    case ${option} in
      "h" ) export f5Hosts=${OPTARG}
            OPTION_H=1
        ;;
      "f" ) OPTION_F=1
        ;;
      "s" ) OPTION_S=1
        ;;
      "v" ) OPTION_V=1
        ;;
      "r" ) export restoreDate=${OPTARG}
            OPTION_R=1
        ;;
      "e" ) export smalsEnv=${OPTARG}
            OPTION_E=1
        ;;
      \?  ) ${ECHO} "Invalid option: ${OPTARG}"
        ShowUsage
        return 1
        ;;
   esac
done

if [[ ${OPTION_E} -eq 0 ]]; then
  echoError "You need to specify an environment using -e"
  ShowUsage
  exit
fi

if [[ ${f5Hosts} == "" ]]; then
  f5Hosts=$(basename -s .tcl ${baseDir}/../parm/instance/*${smalsEnv}*.tcl)
fi

if [[ ${OPTION_S} -ne 0 ]]; then
  f5Hosts=$(echo ${f5Hosts} | awk '{ print $1 }')
fi

for f5Host in ${f5Hosts}; do
  zone=${f5Host:2:3}
  #env=${f5Host:5:3}
  env=${smalsEnv}
  node=${f5Host:8:2}
  localTmp=${baseDir}/.${f5Host}.tcl
  localBackupDir=${baseDir}/../../loc/backup/scf
  remoteTmp=/var/tmp/${f5Host}.tcl
  remoteTmpSCF=/var/tmp/${f5Host}-scf-${DATETIME}
  remoteTmpAsm=/var/tmp/
  remoteRestoreSCF=/var/tmp
  pfInput=${baseDir}/../parm/net/packet-filter/packet-filter.${smalsEnv}.pf
  if [[ ${OPTION_R} -ne 0 ]]; then
    restoreSCF
  else
    statusVault
    if [[ ${OPTION_V} -eq 0 ]]; then
      bacupSCF
    fi
    generateConfig
    parseConfig
    copyConfigLb
    if [[ ${OPTION_V} -ne 0 ]]; then
      verifyConfig
    else
      if [[ ${OPTION_F} -eq 0 ]]; then
        installSysCert
        installCert
        installAsmPolicy
      fi
      installConfig
      if [[ -f ${pfInput} ]]; then
        generatePacketFilter
        updatePacketFilter
      fi
    fi
    cleanUp
    if [[ ${OPTION_F} -eq 0 ]]; then
      echoInfo "Waiting for devices to settle..."
      sleep 30
    fi
  fi
done
echo "F5 CONFIG PUSHED: -tenant $USER ${*:1} " | mailx -E -s "F5 CONFIG PUSHED -tenant $USER ${*:1} " -S smtp=smtp://relay.services.gcloud.belgium.be -S from="do_not_reply@smals.be" reverseproxiesadmin@smals.be


