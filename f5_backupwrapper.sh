#!/bin/ksh

echoError () {
    echo -e "\033[1;31m""ERROR: ${1}""\033[0m"
}

echoWarn () {
    echo -e "\033[0;33m""WARN:  ${1}""\033[0m"
}

echoInfo () {
    echo "INFO:  ${1}"
}

echoOk () {
    echo -e "\033[0;32m""OK:    ${1}""\033[0m"
}

sshOpts="-o StrictHostKeyChecking=no -o CheckHostIP=no -o PasswordAuthentication=no -o Protocol=2 -q"
dataDir=$1

for f5Host in $2; do
  if [[ $(id) != "pf5crp" && ${f5Host} == eap* ]]; then
    # Only backup vCMP hosts from PRD env
    continue
  fi
  if [[ ! -d ${dataDir}/ ]]; then
    mkdir ${dataDir}
    RC=$?
  fi
  if [[ ${RC} -eq 0 ]]; then
    echoInfo "${dataDir} created"
    scp ${sshOpts} f5_backup.sh root@${f5Host}:/var/tmp/f5backup.sh > /dev/null
  else
    echoError "Creation of ${dataDir} failed"
    exit 1
  fi
  RC=$?
  if [[ ${RC} -eq 0 ]]; then
    echoInfo "Backup script copied to ${f5Host}"
    ssh ${sshOpts} root@${f5Host} "tmsh restart sys service restjavad" > /dev/null
    RC=$?
  else
   echoError "Copy of backup script to ${f5Host} failed"
   exit 1
  fi
  if [[ ${RC} -eq 0 ]]; then
    echoInfo "Restarted REST daemon on ${f5Host} (https://support.f5.com/kb/en-us/solutions/public/16000/700/sol16751.html)"
    ssh ${sshOpts} root@${f5Host} "bash -c \"chmod +x /var/tmp/f5backup.sh && /var/tmp/f5backup.sh\"" > /dev/null
    RC=$?
  else
    echoError "Restart of REST daemon on ${f5Host} failed"
    exit 2
  fi
  if [[ ${RC} -eq 0 ]]; then
    echoOk "Backup script executed on ${f5Host}"
    rsync --archive -e "ssh ${sshOpts}" root@${f5Host}:/shared/backups/ ${dataDir} > /dev/null
    RC=$?
  else
    echoError "Execution of backup script on ${f5Host} failed"
    exit 3
  fi
  if [[ ${RC} -eq 0 ]]; then
    echoOk "Successfully transferred files to ${dataDir}"
    find ${dataDir}  -type f -ctime +21 -exec rm -f {} \;
  else
    echoError "Transferring files from ${f5Host} to ${dataDir} failed"
  fi
  if [[ ${RC} -eq 0 ]]; then
    echoInfo "Cleaned old files in ${dataDir}"
  else
    echoError "Cleanup of ${dataDir} failed"
    exit 5
  fi
done
